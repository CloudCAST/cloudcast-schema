# NAME

CloudCAST::Schema - Root module for CloudCAST schema

# SYNOPSIS

    # To deploy the database, from bash

    alias cloudcast-schema='dbic-migration -Ilib \
      --schema_class CloudCAST::Schema --dsn dbi:Pg:'

    export PGUSER=username
    export PGPASSWORD=passwordexport
    export PGDATABASE=database

    cloudcast-schema status  # Check current installed version
    cloudcast-schema prepare # Prepare files for migration
    cloudcast-schema install # Install and populate database from fixtures
    cloudcast-schema upgrade # Upgrade from a previous version of the schema

    unalias cloudcast-schema

# DESCRIPTION

This is a [DBIx::Class](https://metacpan.org/pod/DBIx::Class) schema for the CloudCSAT project. It contains tables
defining all the components needed for the correct functioning of both major
aspects of the project:

## Core Functionality

- [CloudCAST::Schema::Result::User](https://metacpan.org/pod/CloudCAST::Schema::Result::User)

    A user account, and the top-level namespace

- [CloudCAST::Schema::Result::UserKey](https://metacpan.org/pod/CloudCAST::Schema::Result::UserKey)

    An API key for a user, either globally or restricted to an application

- [CloudCAST::Schema::Result::Application](https://metacpan.org/pod/CloudCAST::Schema::Result::Application)

    The second-tier namespace, where most data is organised

## Speech recognition

- [CloudCAST::Schema::Result::Recogniser](https://metacpan.org/pod/CloudCAST::Schema::Result::Recogniser)

    An abstract speech recogniser

- [CloudCAST::Schema::Result::RecognitionModel](https://metacpan.org/pod/CloudCAST::Schema::Result::RecognitionModel)

    A language model, used to specify an abstract recogniser

- [CloudCAST::Schema::Result::Grammar](https://metacpan.org/pod/CloudCAST::Schema::Result::Grammar)

    A language grammar, used to specify an abstract recogniser

- [CloudCAST::Schema::Result::Lexicon](https://metacpan.org/pod/CloudCAST::Schema::Result::Lexicon)

    A lexicon, used to specify an abstract recogniser

- [CloudCAST::Schema::Result::Decoding](https://metacpan.org/pod/CloudCAST::Schema::Result::Decoding)

    An instantiated speech recogniser, used to decode audio

- [CloudCAST::Schema::Result::DecodingResult](https://metacpan.org/pod/CloudCAST::Schema::Result::DecodingResult)

    The result of decoding audio with an instantiated recogniser

## Data Management

- [CloudCAST::Schema::Result::Collection](https://metacpan.org/pod/CloudCAST::Schema::Result::Collection)

    The root table for data sets

- [CloudCAST::Schema::Result::Collection::Session](https://metacpan.org/pod/CloudCAST::Schema::Result::Collection::Session)

    A data collection session

- [CloudCAST::Schema::Result::Collection::Participant](https://metacpan.org/pod/CloudCAST::Schema::Result::Collection::Participant)

    A contributor to a data set

- [CloudCAST::Schema::Result::Collection::Session::Item](https://metacpan.org/pod/CloudCAST::Schema::Result::Collection::Session::Item)

    An individual data entry in the data set

- [CloudCAST::Schema::Result::Meta::\*](https://metacpan.org/pod/CloudCAST::Schema::Result::Meta::*)

    A set of assorted tables with meta information

# ACKNOWLEDGEMENTS

Work on the design of this table (and others in the schema) owes a considerable
amount to the input and feedback from the rest of the CloudCAST team, and in
particular:

- Phil Green (University of Sheffield)
- Heidi Christensen (University of Sheffield)
- Andre Coy (University of West Indies)
- Frank Ludcicz (University of Toronto)
- Massimiliano Malavasi (Ausilioteca AIAS)
- Lorenzo Desideri (Ausilioteca AIAS)
- Ricard Marxer (University of Sheffield)
- Jochen Farwer (University of Sheffield)

# AUTHOR

- José Joaquín Atria <jjatria@cpan.org>

# COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
