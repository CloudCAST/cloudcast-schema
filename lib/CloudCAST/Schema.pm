# ABSTRACT: Root module for CloudCAST schema
package CloudCAST::Schema;

our $VERSION = '3';

# Based on http://stackoverflow.com/a/22483146/807650

use Moose;
use MooseX::MarkAsMethods autoclean => 1;

# based on the DBIx::Class Schema base class
extends 'DBIx::Class::Schema';

# Load table modules
__PACKAGE__->load_namespaces;

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding UTF-8

=head1 NAME

CloudCAST::Schema - Root module for CloudCAST schema

=head1 SYNOPSIS

    # To deploy the database, from bash

    alias cloudcast-schema='dbic-migration -Ilib \
      --schema_class CloudCAST::Schema --dsn dbi:Pg:'

    export PGUSER=username
    export PGPASSWORD=passwordexport
    export PGDATABASE=database

    cloudcast-schema status  # Check current installed version
    cloudcast-schema prepare # Prepare files for migration
    cloudcast-schema install # Install and populate database from fixtures
    cloudcast-schema upgrade # Upgrade from a previous version of the schema

    unalias cloudcast-schema

=head1 DESCRIPTION

This is a L<DBIx::Class> schema for the CloudCSAT project. It contains tables
defining all the components needed for the correct functioning of both major
aspects of the project:

=head2 Core Functionality

=over 4

=item * L<CloudCAST::Schema::Result::User>

A user account, and the top-level namespace

=item * L<CloudCAST::Schema::Result::UserKey>

An API key for a user, either globally or restricted to an application

=item * L<CloudCAST::Schema::Result::Application>

The second-tier namespace, where most data is organised

=back

=head2 Speech recognition

=over 4

=item * L<CloudCAST::Schema::Result::Recogniser>

An abstract speech recogniser

=item * L<CloudCAST::Schema::Result::RecognitionModel>

A language model, used to specify an abstract recogniser

=item * L<CloudCAST::Schema::Result::Grammar>

A language grammar, used to specify an abstract recogniser

=item * L<CloudCAST::Schema::Result::Lexicon>

A lexicon, used to specify an abstract recogniser

=item * L<CloudCAST::Schema::Result::Decoding>

An instantiated speech recogniser, used to decode audio

=item * L<CloudCAST::Schema::Result::DecodingResult>

The result of decoding audio with an instantiated recogniser

=back

=head2 Data Management

=over 4

=item * L<CloudCAST::Schema::Result::Collection>

The root table for data sets

=item * L<CloudCAST::Schema::Result::Collection::Session>

A data collection session

=item * L<CloudCAST::Schema::Result::Collection::Participant>

A contributor to a data set

=item * L<CloudCAST::Schema::Result::Collection::Session::Item>

An individual data entry in the data set

=item * L<CloudCAST::Schema::Result::Meta::*>

A set of assorted tables with meta information

=back

=head1 ACKNOWLEDGEMENTS

Work on the design of this table (and others in the schema) owes a considerable
amount to the input and feedback from the rest of the CloudCAST team, and in
particular:

=over 4

=item * Phil Green (University of Sheffield)

=item * Heidi Christensen (University of Sheffield)

=item * Andre Coy (University of West Indies)

=item * Frank Ludcicz (University of Toronto)

=item * Massimiliano Malavasi (Ausilioteca AIAS)

=item * Lorenzo Desideri (Ausilioteca AIAS)

=item * Ricard Marxer (University of Sheffield)

=item * Jochen Farwer (University of Sheffield)

=back

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
