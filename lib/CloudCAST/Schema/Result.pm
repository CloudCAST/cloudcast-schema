package CloudCAST::Schema::Result;

our $VERSION = '0';

use Moose;
use MooseX::MarkAsMethods autoclean => 1;
use MooseX::NonMoose;
use Ref::Util qw( is_blessed_hashref );

extends 'DBIx::Class::Core';

# this is the right place to implement generic stuff

# DBIx::Class::Cookbook recommends loading components in a central place
__PACKAGE__->load_components(qw/
  InflateColumn::DateTime
  InflateColumn::Serializer
  DynamicDefault
  TimeStamp
  Core
/);

sub uri_name {
  my $name = $_[0]->name;
  return $name unless $_[0]->can('namespace');
  return $_[0]->namespace . '/' . $name;
}

sub render {
  my $self = shift;
  my @keys = (@_) ? @_ : $self->columns;

  my $render;
  foreach (@keys) {
    my $val = $self->$_;
    $val = $val->render if is_blessed_hashref($val) and $val->can('render');
    $render->{$_} = $val if defined $val;
  }

  return $render;
}

__PACKAGE__->meta->make_immutable;

1;
