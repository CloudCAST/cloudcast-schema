package CloudCAST::Schema::Result::Application;

our $VERSION = '0';

use Moose;
use MooseX::MarkAsMethods autoclean => 1;

extends 'CloudCAST::Schema::Result';

__PACKAGE__->table('application');

__PACKAGE__->add_columns(
  id => {
    data_type => 'integer',
    is_numeric => 1,
    size => 10,
    is_auto_increment => 1,
    extra => {
      unsigned => 1,
    },
  },

  name => {
    data_type => 'varchar',
    size => 255,
    is_nullable => 1,
    dynamic_default_on_create => sub {
      require String::CamelSnakeKebab;
      String::CamelSnakeKebab::kebab_case $_[0]->display_name;
    },
  },

  owner => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  parent => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    is_nullable => 1,
    extra => {
      unsigned => 1,
    },
  },

  display_name => {
    data_type => 'varchar',
    size => 255,
    is_nullable => 1,
    dynamic_default_on_create => sub { $_[0]->name },
  },

  description => {
    data_type => 'varchar',
    size => 255,
    is_nullable => 1,
  },

  visibility => {
    data_type => 'integer',
    size => 10,
    is_nullable => 0,
    dynamic_default_on_create => sub { 0 },
    extra => {
      unsigned => 1,
    },
  },

  default_decoding => {
    data_type => 'integer',
    is_numeric => 1,
    is_nullable => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  date_created => {
    data_type => 'datetime',
    set_on_create => 1,
  },

  date_updated => {
    data_type => 'datetime',
    set_on_create => 1,
    set_on_update => 1,
  },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->add_unique_constraint([ qw( owner name ) ]);

__PACKAGE__->belongs_to(
  owner => 'CloudCAST::Schema::Result::User', undef,
  { join_type => 'left' },
);

__PACKAGE__->belongs_to(
  parent => 'CloudCAST::Schema::Result::Application', undef,
  { join_type => 'left' },
);

__PACKAGE__->belongs_to(
  default_decoding => 'CloudCAST::Schema::Result::Decoding', undef,
  { join_type => 'left' },
);

__PACKAGE__->has_many(
  application_users => 'CloudCAST::Schema::Result::ApplicationUser','application',
  { cascade_delete => 1, cascade_copy => 0 },
);

__PACKAGE__->has_many(
  decodings => 'CloudCAST::Schema::Result::Decoding', 'application',
);

__PACKAGE__->has_many(
  recognisers => 'CloudCAST::Schema::Result::Recogniser', 'application',
);

__PACKAGE__->has_many(
  grammars => 'CloudCAST::Schema::Result::Grammar', 'application',
);

__PACKAGE__->has_many(
  lexicons => 'CloudCAST::Schema::Result::Lexicon', 'application',
);

__PACKAGE__->has_many(
  models => 'CloudCAST::Schema::Result::RecognitionModel', 'application',
);

# As per http://web.archive.org/web/20070204122440/dbix-class.shadowcatsystems.co.uk/index.cgi?ManyToManyWithAttributes
sub users {
  my ($self) = @_;
  return $self->application_users({}, { prefetch => ['member'] });
}

sub namespace { return shift->owner->name }

around render => sub {
  my $orig = shift;
  my $self = shift;

  my $render = $self->$orig(
    qw( name display_name namespace description visibility )
  );

  my $parent = $self->parent;
  $render->{parent} = $parent->uri_name if $parent;

  my $default = $self->default_decoding;
  $render->{default_decoding} = $default->name if $default;

  $render->{users} = [
    map {
      {
        name => $_->member->name,
        role => $_->role->name,
      }
    } $self->users
  ];

  return $render;
};

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding UTF-8

=head1 NAME

CloudCAST::Schema::Result::Application - A CloudCAST application

=head1 DESCRIPTION

Objects of this class represent an application in the CloudCAST system. An
application is the basic namespace where operations are possible. They specify
a name under which user data, speech data, and speech recognition options,
among other preferences, can be stored and accessed.

=head1 ACKNOWLEDGEMENTS

Work on the design of this table (and others in the schema) owes a considerable
amount to the input and feedback from the rest of the CloudCAST team, and in
particular:

=over 4

=item * Phil Green (University of Sheffield)

=item * Heidi Christensen (University of Sheffield)

=item * Andre Coy (University of West Indies)

=item * Frank Ludcicz (University of Toronto)

=item * Massimiliano Malavasi (Ausilioteca AIAS)

=item * Lorenzo Desideri (Ausilioteca AIAS)

=item * Ricard Marxer (University of Sheffield)

=item * Jochen Farwer (University of Sheffield)

=back

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
