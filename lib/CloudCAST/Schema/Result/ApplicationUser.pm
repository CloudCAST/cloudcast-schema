package CloudCAST::Schema::Result::ApplicationUser;

our $VERSION = '0';

use Moose;
use MooseX::MarkAsMethods autoclean => 1;

extends 'CloudCAST::Schema::Result';

__PACKAGE__->table('application_user');

__PACKAGE__->add_columns(
  member => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  recogniser => {
    data_type => 'integer',
    is_numeric => 1,
    is_nullable => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  application => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  role => {
    data_type => 'varchar',
    size => 10,
    is_foreign_key => 1,
  },
);

__PACKAGE__->belongs_to(
  member => 'CloudCAST::Schema::Result::User', undef,
  { proxy => [qw(
    id name email_address first_name last_name
    password password_salt hash_method
    date_created date_updated
  )] },
);

__PACKAGE__->belongs_to(
  application => 'CloudCAST::Schema::Result::Application', undef,
  { join_type => 'left' },
);

__PACKAGE__->belongs_to(
  recogniser => 'CloudCAST::Schema::Result::Recogniser', undef,
  { join_type => 'left' },
);

__PACKAGE__->belongs_to(
  role => 'CloudCAST::Schema::Result::Meta::AccessLevel', undef,
  { join_type => 'left' },
);

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding UTF-8

=head1 NAME

CloudCAST::Schema::Result::ApplicationUser - The user in an application

=head1 DESCRIPTION

This table makes it possible to have a one-to-many relationship between
users (see L<CloudCAST::Schema::Result::User>) and applications
(see L<CloudAST::Schema::Result::Application>).

=head1 ACKNOWLEDGEMENTS

Work on the design of this table (and others in the schema) owes a considerable
amount to the input and feedback from the rest of the CloudCAST team, and in
particular:

=over 4

=item * Phil Green (University of Sheffield)

=item * Heidi Christensen (University of Sheffield)

=item * Andre Coy (University of West Indies)

=item * Frank Ludcicz (University of Toronto)

=item * Massimiliano Malavasi (Ausilioteca AIAS)

=item * Lorenzo Desideri (Ausilioteca AIAS)

=item * Ricard Marxer (University of Sheffield)

=item * Jochen Farwer (University of Sheffield)

=back

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
