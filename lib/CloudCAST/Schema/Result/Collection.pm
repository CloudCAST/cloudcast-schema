package CloudCAST::Schema::Result::Collection;

our $VERSION = '0';

use Moose;
use MooseX::MarkAsMethods autoclean => 1;

extends 'CloudCAST::Schema::Result';

__PACKAGE__->table('collection');

__PACKAGE__->add_columns(
  id => {
    data_type => 'integer',
    is_numeric => 1,
    size => 10,
    is_auto_increment => 1,
    extra => {
      unsigned => 1,
    },
  },

  name => {
    data_type => 'varchar',
    size => 200,
  },

  institution => {
    data_type => 'varchar',
    size => 200,
    is_nullable => 1,
  },

  city => {
    data_type => 'varchar',
    size => 255,
    is_nullable => 1,
  },

  country => {
    data_type => 'varchar',
    size => 2,
    is_nullable => 1,
    is_foreign_key => 1,
  },

  license => {
    data_type => 'varchar',
    size => 200,
    is_nullable => 1,
  },

  contact_name => {
    data_type => 'varchar',
    size => 100,
    is_nullable => 1,
  },

  contact_email => {
    data_type => 'varchar',
    size => 100,
    is_nullable => 1,
  },

  contact_phone => {
    data_type => 'varchar',
    size => 30,
    is_nullable => 1,
  },

  contact_address => {
    data_type => 'varchar',
    size => 100,
    is_nullable => 1,
  },

  date_created => {
    data_type => 'datetime',
    set_on_create => 1,
  },

  date_updated => {
    data_type => 'datetime',
    set_on_create => 1,
    set_on_update => 1,
  },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
  sessions => 'CloudCAST::Schema::Result::Collection::Session', 'collection',
  { cascade_delete => 1 },
);

__PACKAGE__->has_many(
  participants => 'CloudCAST::Schema::Result::Collection::Participant', 'collection',
  { cascade_delete => 1 },
);

__PACKAGE__->belongs_to(
  country => 'CloudCAST::Schema::Result::Meta::Country', undef,
  { join_type => 'left' },
);

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding UTF-8

=head1 NAME

CloudCAST::Schema::Result::Collection - The top-level for a data set

=head1 DESCRIPTION

This class represents a data set, that can be used for training recognition
models, or simply queried for data by a user with the sufficient privileges.

This top-level holds a number of sessions
(see L<CloudCAST::Schema::Result::Collection::Session>),
and a list of all the participants that contributed to that data set
(see L<CloudCAST::Schema::Result::Collection::Participant>).

Individual data entries are contained in the sessions during which they were
collected (see L<CloudCAST::Schema::Result::Collection::Session::Item>).

=head1 ACKNOWLEDGEMENTS

The definition for this table (and those related to it) is based on the
previous work by the following collaborators:

=over 4

=item * Maria Yancheva (University of Toronto)

=item * Frank Ludcicz (University of Toronto)

=item * Heidi Christensen (University of Sheffield)

=back

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
