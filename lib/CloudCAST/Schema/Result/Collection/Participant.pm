package CloudCAST::Schema::Result::Collection::Participant;

our $VERSION = '0';

use Moose;
use MooseX::MarkAsMethods autoclean => 1;

extends 'CloudCAST::Schema::Result';

__PACKAGE__->table('collection_participant');

__PACKAGE__->add_columns(
  id => {
    data_type => 'integer',
    is_numeric => 1,
    size => 10,
    is_auto_increment => 1,
    extra => {
      unsigned => 1,
    },
  },

  collection => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  first_name => {
    data_type => 'varchar',
    size => 50,
    is_nullable => 1,
  },

  last_name => {
    data_type => 'varchar',
    size => 50,
    is_nullable => 1,
  },

  consent_substitute => {
    data_type => 'boolean',
    default_value => 0,
  },

  consent_substitute_name => {
    data_type => 'varchar',
    size => 100,
    default_value => '',
  },

  gender => {
    data_type => 'char',
    size => 1,
    is_foreign_key => 1,
  },

  age_year => {
    data_type => 'integer',
    size => 3,
    is_nullable => 1,
  },

  age_month => {
    data_type => 'integer',
    size => 4,
    is_nullable => 1,
  },

  dob => {
    data_type => 'datetime',
    is_nullable => 1,
  },

  dob_month => {
    data_type => 'integer',
    size => 2,
    is_nullable => 1,
  },

  dob_year => {
    data_type => 'integer',
    size => 4,
    is_nullable => 1,
  },

  education_year => {
    data_type => 'integer',
    size => 3,
    is_foreign_key => 1,
  },

  native_language => {
    data_type => 'varchar',
    size => 2,
    is_foreign_key => 1,
  },

  contact_email => {
    data_type => 'varchar',
    size => 100,
    is_nullable => 1,
  },

  contact_phone => {
    data_type => 'varchar',
    size => 30,
    is_nullable => 1,
  },

  contact_address => {
    data_type => 'varchar',
    size => 100,
    is_nullable => 1,
  },

  diagnosis => {
    data_type => 'varchar',
    size => 255,
    is_nullable => 1,
  },

  diagnosis_code => {
    data_type => 'varchar',
    size => 50,
    is_nullable => 1,
  },

  score_mmse => {
    data_type => 'integer',
    size => 2,
    is_nullable => 1,
  },

  score_moca => {
    data_type => 'integer',
    size => 2,
    is_nullable => 1,
  },

  score_updrs => {
    data_type => 'integer',
    size => 2,
    is_nullable => 1,
  },

  score_hrsd => {
    data_type => 'integer',
    size => 2,
    is_nullable => 1,
  },

  score_gsd => {
    data_type => 'integer',
    size => 2,
    is_nullable => 1,
  },

  date_created => {
    data_type => 'datetime',
    set_on_create => 1,
  },

  date_updated => {
    data_type => 'datetime',
    set_on_create => 1,
    set_on_update => 1,
  },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
  items => 'CloudCAST::Schema::Result::Collection::Session::Item', 'participant',
  { cascade_delete => 1 },
);

__PACKAGE__->belongs_to(
  collection => 'CloudCAST::Schema::Result::Collection', undef,
  { join_type => 'left' },
);

__PACKAGE__->belongs_to(
  gender => 'CloudCAST::Schema::Result::Meta::Gender', undef,
  { join_type => 'left' },
);

__PACKAGE__->belongs_to(
  native_language => 'CloudCAST::Schema::Result::Meta::Language', undef,
  { join_type => 'left' },
);

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding UTF-8

=head1 NAME

CloudCAST::Schema::Result::Collection::Participant - A contributor to a data set

=head1 DESCRIPTION

This class represents an individual who contributed one or more data entries
(see L<CloudCAST::Schema::Result::Collection::Session::Item>)
to a given data set
(see L<CloudCAST::Schema::Result::Collection>).

However, this class represents contributors to a specific collection, and not
individuals in general. So the data contained here only makes sense I<within
the data set to which it belongs>.

This means that even if the same actually existing person contributed data to
more than one collection, each of those collections should have is own
representation of that contributor.

The contributor represented by this class is also entirely unrelated to a
CloudCAST user (see L<CloudCAST::Schema::Result::User>).

=head1 ACKNOWLEDGEMENTS

The definition for this table (and those related to it) is based on the
previous work by the following collaborators:

=over 4

=item * Maria Yancheva (University of Toronto)

=item * Frank Ludcicz (University of Toronto)

=item * Heidi Christensen (University of Sheffield)

=back

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
