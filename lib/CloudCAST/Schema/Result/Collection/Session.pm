package CloudCAST::Schema::Result::Collection::Session;

our $VERSION = '0';

use Moose;
use MooseX::MarkAsMethods autoclean => 1;

extends 'CloudCAST::Schema::Result';

__PACKAGE__->table('collection_session');

__PACKAGE__->add_columns(
  id => {
    data_type => 'integer',
    is_numeric => 1,
    size => 10,
    is_auto_increment => 1,
    extra => {
      unsigned => 1,
    },
  },

  collection => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  date_started => {
    data_type => 'datetime',
  },

  date_ended => {
    data_type => 'datetime',
    is_nullable => 1,
  },

  equipment => {
    data_type => 'varchar',
    size => 500,
  },

  location => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  country => {
    data_type => 'varchar',
    size => 2,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  city => {
    data_type => 'varchar',
    size => 255,
  },

  date_created => {
    data_type => 'datetime',
    set_on_create => 1,
  },

  date_updated => {
    data_type => 'datetime',
    set_on_create => 1,
    set_on_update => 1,
  },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
  items => 'CloudCAST::Schema::Result::Collection::Session::Item', 'session',
  { cascade_delete => 1 },
);

__PACKAGE__->belongs_to(
  collection => 'CloudCAST::Schema::Result::Collection', undef,
  { join_type => 'left' },
);

__PACKAGE__->belongs_to(
  country => 'CloudCAST::Schema::Result::Meta::Country', undef,
  { join_type => 'left' },
);

__PACKAGE__->belongs_to(
  location => 'CloudCAST::Schema::Result::Meta::Location', undef,
  { join_type => 'left' },
);

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding UTF-8

=head1 NAME

CloudCAST::Schema::Result::Collection::Session - A data collection session

=head1 DESCRIPTION

This class represents set of data entries
(see L<CloudCAST::Schema::Result::Collection::Session::Item>)
each of which belongs to a particular data set
(see L<CloudCAST::Schema::Result::Collection>).

=head1 ACKNOWLEDGEMENTS

The definition for this table (and those related to it) is based on the
previous work by the following collaborators:

=over 4

=item * Maria Yancheva (University of Toronto)

=item * Frank Ludcicz (University of Toronto)

=item * Heidi Christensen (University of Sheffield)

=back

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
