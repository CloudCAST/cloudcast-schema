package CloudCAST::Schema::Result::Collection::Session::Item;

our $VERSION = '0';

use Moose;
use MooseX::MarkAsMethods autoclean => 1;

extends 'CloudCAST::Schema::Result';

__PACKAGE__->table('collection_session_item');

__PACKAGE__->add_columns(
  id => {
    data_type => 'integer',
    is_numeric => 1,
    size => 10,
    is_auto_increment => 1,
    extra => {
      unsigned => 1,
    },
  },

  collection => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  session => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  participant => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  date_started => {
    data_type => 'datetime',
  },

  date_ended => {
    data_type => 'datetime',
    is_nullable => 1,
  },

  type => {
    data_type => 'char',
    size => 3,
    is_foreign_key => 1,
  },

  data => {
    data_type => 'longtext',
  },

  date_created => {
    data_type => 'datetime',
    set_on_create => 1,
  },

  date_updated => {
    data_type => 'datetime',
    set_on_create => 1,
    set_on_update => 1,
  },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
  collection => 'CloudCAST::Schema::Result::Collection', undef,
  { join_type => 'left' },
);

__PACKAGE__->belongs_to(
  session => 'CloudCAST::Schema::Result::Collection::Session', undef,
  { join_type => 'left' },
);

__PACKAGE__->belongs_to(
  participant => 'CloudCAST::Schema::Result::Collection::Participant', undef,
  { join_type => 'left' },
);

__PACKAGE__->belongs_to(
  type => 'CloudCAST::Schema::Result::Meta::DataType', undef,
  { join_type => 'left' },
);

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding UTF-8

=head1 NAME

CloudCAST::Schema::Result::Collection::Session::Item - An entry in a data set

=head1 DESCRIPTION

This class represents an individual entry in a data set
(see L<CloudCAST::Schema::Result::Collection>). In addition to the actual data
(which can be any of audio, video, text, or a combination thereof; see
L<CloudCAST::Schema::Result::Meta::DataType>), objects of this class will be
linked to the participant who contributed the data
(see L<CloudCAST::Schema::Result::Collection::Participant>) and the session
during which the recording took place
(see L<CloudCAST::Schema::Result::Collection::Session>).

=head1 ACKNOWLEDGEMENTS

The definition for this table (and those related to it) is based on the
previous work by the following collaborators:

=over 4

=item * Maria Yancheva (University of Toronto)

=item * Frank Ludcicz (University of Toronto)

=item * Heidi Christensen (University of Sheffield)

=back

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
