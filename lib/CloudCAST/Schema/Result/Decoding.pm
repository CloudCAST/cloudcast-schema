package CloudCAST::Schema::Result::Decoding;

our $VERSION = '0';

use Moose;
use MooseX::MarkAsMethods autoclean => 1;

extends 'CloudCAST::Schema::Result';

__PACKAGE__->table('decoding');

__PACKAGE__->add_columns(
  id => {
    data_type => 'integer',
    size => 10,
    is_auto_increment => 1,
    extra => {
      unsigned => 1,
    },
  },

  name => {
    data_type => 'varchar',
    size => 100,
    is_nullable => 1,
  },

  recogniser => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  application => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  parameters => {
    data_type => 'json',
    serializer_class => 'JSON',
  },

  date_created => {
    data_type => 'datetime',
    set_on_create => 1,
  },

  date_updated => {
    data_type => 'datetime',
    set_on_create => 1,
    set_on_update => 1,
  },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->add_unique_constraint([ qw( application name ) ]);

__PACKAGE__->has_many(
  results => 'CloudCAST::Schema::Result::DecodingResult', 'decoding',
);

__PACKAGE__->belongs_to(
  recogniser => 'CloudCAST::Schema::Result::Recogniser', undef,
  { join_type => 'left' },
);

__PACKAGE__->belongs_to(
  application => 'CloudCAST::Schema::Result::Application', undef,
  { join_type => 'left' },
);

sub merged_parameters {
  my ($self) = @_;

  use Hash::Merge::Simple qw( clone_merge );
  clone_merge
    $self->recogniser->merged_parameters,
    $self->parameters;
}

sub namespace {
  my $app = shift->application;
  return join '/', $app->namespace, $app->name;
}

around render => sub {
  my $orig = shift;
  my $self = shift;

  my $r = $self->recogniser;

  return {
    name       => $self->name,
    recogniser => {
      name => $r->name,
      type => $r->type,
      model => $r->model->name,
    },
    namespace  => $self->namespace,
    parameters => $self->merged_parameters,
  };
};

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding UTF-8

=head1 NAME

CloudCAST::Schema::Result::Decoding - An instantiated recogniser

=head1 DESCRIPTION

This class represents the combination of a particular speech recogniser (see
L<CloudCAST::Schema::Result::Recogniser>), with a set of parameters. Once thus
instantiated, this I<decoding> can be used to decode audio data.

Objects of this class hold references to any results that it has generated in
the past (see L<CloudCAST::Scheme::Result::DecodingResult>).

=head1 ACKNOWLEDGEMENTS

Work on the design of this table (and others in the schema) owes a considerable
amount to the input and feedback from the rest of the CloudCAST team, and in
particular:

=over 4

=item * Phil Green (University of Sheffield)

=item * Heidi Christensen (University of Sheffield)

=item * Andre Coy (University of West Indies)

=item * Frank Ludcicz (University of Toronto)

=item * Massimiliano Malavasi (Ausilioteca AIAS)

=item * Lorenzo Desideri (Ausilioteca AIAS)

=item * Ricard Marxer (University of Sheffield)

=item * Jochen Farwer (University of Sheffield)

=back

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
