package CloudCAST::Schema::Result::DecodingResult;

our $VERSION = '0';

use Moose;
use MooseX::MarkAsMethods autoclean => 1;

extends 'CloudCAST::Schema::Result';

__PACKAGE__->table('decoding_result');

__PACKAGE__->add_columns(
  input => {
    data_type => 'varchar',
    size => 100,
  },

  input_type => {
    data_type => 'varchar',
    is_foreign_key => 1,
    size => 100,
  },

  output => {
    data_type => 'json',
    serializer_class => 'JSON',
  },

  decoding => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  date_created => {
    data_type => 'datetime',
    set_on_create => 1,
  },
);

__PACKAGE__->belongs_to(
  decoding => 'CloudCAST::Schema::Result::Decoding', undef,
  { join_type => 'left' },
);

__PACKAGE__->belongs_to(
  input_type => 'CloudCAST::Schema::Result::Meta::DataType', undef,
  { join_type => 'left' },
);

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding UTF-8

=head1 NAME

CloudCAST::Schema::Result::DecodingResult - The result of a specific decoding

=head1 DESCRIPTION

This class represents the results of a specific decoding session. It stores the
final output generated for said decoding, as well as a link to the parameters
used for that decoding, to allow for future analysis and review (see
L<CloudCAST::Schema::Result::Decoding>).

=head1 ACKNOWLEDGEMENTS

Work on the design of this table (and others in the schema) owes a considerable
amount to the input and feedback from the rest of the CloudCAST team, and in
particular:

=over 4

=item * Phil Green (University of Sheffield)

=item * Heidi Christensen (University of Sheffield)

=item * Andre Coy (University of West Indies)

=item * Frank Ludcicz (University of Toronto)

=item * Massimiliano Malavasi (Ausilioteca AIAS)

=item * Lorenzo Desideri (Ausilioteca AIAS)

=item * Ricard Marxer (University of Sheffield)

=item * Jochen Farwer (University of Sheffield)

=back

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
