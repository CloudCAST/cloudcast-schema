package CloudCAST::Schema::Result::GrammarLexicon;

our $VERSION = '0';

use Moose;
use MooseX::MarkAsMethods autoclean => 1;

extends 'CloudCAST::Schema::Result';

__PACKAGE__->table('grammar_lexicon');

__PACKAGE__->add_columns(
  grammar => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  lexicon => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

);

__PACKAGE__->belongs_to(
  grammar => 'CloudCAST::Schema::Result::Grammar', undef,
  { join_type => 'left' },
);

__PACKAGE__->belongs_to(
  lexicon => 'CloudCAST::Schema::Result::Lexicon', undef,
  { join_type => 'left' },
);

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding UTF-8

=head1 NAME

CloudCAST::Schema::Result::GrammarLexicon - The link between a grammar and a lexicon

=head1 DESCRIPTION

This class exists only to make it possible to have many-to-many relationships
between grammar and lexicon objects.

For more details, see L<CloudCAST::Schema::Result::Grammar> and
L<CloudCAST::Schema::Result::Lexicon>.

=head1 ACKNOWLEDGEMENTS

Work on the design of this table (and others in the schema) owes a considerable
amount to the input and feedback from the rest of the CloudCAST team, and in
particular:

=over 4

=item * Phil Green (University of Sheffield)

=item * Heidi Christensen (University of Sheffield)

=item * Andre Coy (University of West Indies)

=item * Frank Ludcicz (University of Toronto)

=item * Massimiliano Malavasi (Ausilioteca AIAS)

=item * Lorenzo Desideri (Ausilioteca AIAS)

=item * Ricard Marxer (University of Sheffield)

=item * Jochen Farwer (University of Sheffield)

=back

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
