package CloudCAST::Schema::Result::Meta::AccessLevel;

our $VERSION = '0';

use Moose;
use MooseX::MarkAsMethods autoclean => 1;

extends 'CloudCAST::Schema::Result';

__PACKAGE__->table('meta_access_level');

__PACKAGE__->add_columns(
  name => {
    data_type => 'varchar',
    size => 10,
  },

  description => {
    data_type => 'text',
  },

  is_admin => {
    data_type => 'boolean',
    default_value => 0,
  },

  can_train_model => {
    data_type => 'boolean',
    default_value => 0,
  },

  can_upload => {
    data_type => 'boolean',
    default_value => 0,
  },

  can_download => {
    data_type => 'boolean',
    default_value => 0,
  },

  can_write_owner => {
    data_type => 'boolean',
    default_value => 0,
  },

  can_write_manager => {
    data_type => 'boolean',
    default_value => 0,
  },

  can_write_user => {
    data_type => 'boolean',
    default_value => 0,
  },

  date_created => {
    data_type => 'datetime',
  },

  date_updated => {
    data_type => 'datetime',
    set_on_update => 1,
  },
);

__PACKAGE__->set_primary_key('name');

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding UTF-8

=head1 NAME

CloudCAST::Schema::Meta::AccessLevel - User permissions

=head1 DESCRIPTION

This table defines the set of permissions that are available for users. Most
commonly, these permissions apply to users (see L<CloudCAST::Schema::User>)
and their roles within applications (see L<CloudCAST::Schema::Application>).

=head1 ACKNOWLEDGEMENTS

The definition for this table (and those related to it) is based on the
previous work by the following collaborators:

=over 4

=item * Maria Yancheva (University of Toronto)

=item * Frank Ludcicz (University of Toronto)

=item * Heidi Christensen (University of Sheffield)

=back

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
