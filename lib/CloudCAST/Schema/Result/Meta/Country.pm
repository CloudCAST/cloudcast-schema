package CloudCAST::Schema::Result::Meta::Country;

our $VERSION = '0';

use Moose;
use MooseX::MarkAsMethods autoclean => 1;

extends 'CloudCAST::Schema::Result';

__PACKAGE__->table('meta_country');

__PACKAGE__->add_columns(
  id => {
    data_type => 'varchar',
    size => 2,
  },

  name => {
    data_type => 'varchar',
    size => 255,
  },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
  sessions => 'CloudCAST::Schema::Result::Collection::Session', 'collection',
  { cascade_delete => 1 },
);

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding UTF-8

=head1 NAME

CloudCAST::Schema::Meta::Country - A list of countries

=head1 DESCRIPTION

This table defines a list of countries and their 2-letter codes. The codes
are those of the ISO 3166-1 alpha-2.

=head1 ACKNOWLEDGEMENTS

The definition for this table (and those related to it) is based on the
previous work by the following collaborators:

=over 4

=item * Maria Yancheva (University of Toronto)

=item * Frank Ludcicz (University of Toronto)

=item * Heidi Christensen (University of Sheffield)

=back

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
