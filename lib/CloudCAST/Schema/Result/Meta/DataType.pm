package CloudCAST::Schema::Result::Meta::DataType;

our $VERSION = '0';

use Moose;
use MooseX::MarkAsMethods autoclean => 1;

extends 'CloudCAST::Schema::Result';

__PACKAGE__->table('meta_data_type');

__PACKAGE__->add_columns(
  id => {
    data_type => 'char',
    size => 3,
  },

  name => {
    data_type => 'varchar',
    size => 100,
  },

  is_text => {
    data_type => 'boolean',
  },

  is_audio => {
    data_type => 'boolean',
  },

  is_video => {
    data_type => 'boolean',
  },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding UTF-8

=head1 NAME

CloudCAST::Schema::Meta::DataType - A set of data types

=head1 DESCRIPTION

This table defines a set of data types. Data is defined by the type of
information it holds: video, audio, text, or a combination thereof.

Data types are identified by a three-character string repreenting the types of
data that are available. The first character will be C<a> if the data has audio,
the second will be a <v> if the data has video, and the third will be C<t> if
the data has text. If any of these are not present, the corresponding character
will be replaced with a hyphen (C<->).

The C<name> field holds a human readable version of the data type, in English.

=head1 ACKNOWLEDGEMENTS

The definition for this table (and those related to it) is based on the
previous work by the following collaborators:

=over 4

=item * Maria Yancheva (University of Toronto)

=item * Frank Ludcicz (University of Toronto)

=item * Heidi Christensen (University of Sheffield)

=back

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
