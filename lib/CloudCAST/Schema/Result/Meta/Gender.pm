package CloudCAST::Schema::Result::Meta::Gender;

our $VERSION = '0';

use Moose;
use MooseX::MarkAsMethods autoclean => 1;

extends 'CloudCAST::Schema::Result';

__PACKAGE__->table('meta_gender');

__PACKAGE__->add_columns(
  id => {
    data_type => 'char',
    size => 1,
  },

  name => {
    data_type => 'varchar',
    size => 20,
  },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding UTF-8

=head1 NAME

CloudCAST::Schema::Meta::Gender - A list of recognised gender flags

=head1 DESCRIPTION

This table holds a list of gender flags to be applied to users and/or
participants
(see L<CloudCAST::Schema::Result::User> and
L<CloudCAST::Schema::Result::Collection::Participant>).

As currently populated, the flags are labeled as being "gender" flags, but they
are more accurately described as "biological sex" flags, and even then they are
only an (intrinsecally insufficient) attempt at providing usable categories.

It is expected that future versions of this table can be populated with more
informative and less problematic approaches.

=head1 ACKNOWLEDGEMENTS

The definition for this table (and those related to it) is based on the
previous work by the following collaborators:

=over 4

=item * Maria Yancheva (University of Toronto)

=item * Frank Ludcicz (University of Toronto)

=item * Heidi Christensen (University of Sheffield)

=back

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
