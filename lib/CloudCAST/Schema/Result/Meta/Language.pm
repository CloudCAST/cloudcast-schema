package CloudCAST::Schema::Result::Meta::Language;

our $VERSION = '0';

use Moose;
use MooseX::MarkAsMethods autoclean => 1;

extends 'CloudCAST::Schema::Result';

__PACKAGE__->table('meta_language');

__PACKAGE__->add_columns(
  id => {
    data_type => 'varchar',
    size => 2,
  },

  name => {
    data_type => 'varchar',
    size => 60,
  },
);


__PACKAGE__->set_primary_key('id');

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding UTF-8

=head1 NAME

CloudCAST::Schema::Meta::Language - A list of language flags

=head1 DESCRIPTION

This table holds a list of languages with their names and their 2-letter codes,
following ISO 639-2.

=head1 ACKNOWLEDGEMENTS

The definition for this table (and those related to it) is based on the
previous work by the following collaborators:

=over 4

=item * Maria Yancheva (University of Toronto)

=item * Frank Ludcicz (University of Toronto)

=item * Heidi Christensen (University of Sheffield)

=back

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
