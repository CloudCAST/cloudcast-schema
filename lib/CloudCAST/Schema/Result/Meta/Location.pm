package CloudCAST::Schema::Result::Meta::Location;

our $VERSION = '0';

use Moose;
use MooseX::MarkAsMethods autoclean => 1;

extends 'CloudCAST::Schema::Result';

__PACKAGE__->table('meta_location');

__PACKAGE__->add_columns(
  id => {
    data_type => 'integer',
    size => 10,
    is_auto_increment => 1,
    extra => {
      unsigned => 1,
    },
  },

  name => {
    data_type => 'varchar',
    size => 20,
  },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding UTF-8

=head1 NAME

CloudCAST::Schema::Meta::Location - A list of possible location types

=head1 DESCRIPTION

This table holds a list of location types to specify the general type of
location where a particular data collection session took place
(see L<CloudCAST::Schema::Result::Collection::Session>).

Initial population of this table includes only two flags to "home" and "lab",
but this list is expected to grow as more data sets, with more specific needs,
are added.

=head1 ACKNOWLEDGEMENTS

The definition for this table (and those related to it) is based on the
previous work by the following collaborators:

=over 4

=item * Maria Yancheva (University of Toronto)

=item * Frank Ludcicz (University of Toronto)

=item * Heidi Christensen (University of Sheffield)

=back

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
