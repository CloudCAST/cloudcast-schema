package CloudCAST::Schema::Result::Recogniser;

our $VERSION = '0';

use Moose;
use MooseX::MarkAsMethods autoclean => 1;

extends 'CloudCAST::Schema::Result';

__PACKAGE__->table('recogniser');

__PACKAGE__->add_columns(
  id => {
    data_type => 'integer',
    size => 10,
    is_auto_increment => 1,
    extra => {
      unsigned => 1,
    },
  },

  name => {
    data_type => 'varchar',
    size => 100,
  },

  application => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  type => {
    data_type => 'varchar',
    size => 100,
  },

  grammar => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  lexicon => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  model => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  visibility => {
    data_type => 'integer',
    size => 10,
    dynamic_default_on_create => sub { 0 },
    extra => {
      unsigned => 1,
    },
  },

  parameters => {
    data_type => 'json',
    serializer_class => 'JSON',
  },

  data_set => {
    data_type => 'json',
    serializer_class => 'JSON',
  },

  date_created => {
    data_type => 'datetime',
    set_on_create => 1,
  },

  date_updated => {
    data_type => 'datetime',
    set_on_create => 1,
    set_on_update => 1,
  },

  date_trained => {
    data_type => 'datetime',
    is_nullable => 1,
  },

  training_scheduled => {
    data_type => 'boolean',
  },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->add_unique_constraint([ qw( application name ) ]);

__PACKAGE__->belongs_to(
  application => 'CloudCAST::Schema::Result::Application', undef,
  { join_type => 'left' },
);

__PACKAGE__->has_many(
  decodings => 'CloudCAST::Schema::Result::Decoding', 'recogniser',
#   { cascade_delete => 1 },
);

__PACKAGE__->belongs_to(
  grammar => 'CloudCAST::Schema::Result::Grammar', undef,
  { join_type => 'left' },
);

__PACKAGE__->belongs_to(
  lexicon => 'CloudCAST::Schema::Result::Lexicon', undef,
  { join_type => 'left' },
);

__PACKAGE__->belongs_to(
  model => 'CloudCAST::Schema::Result::RecognitionModel', undef,
  { join_type => 'left' },
);

sub new_decoding {
  my $self = shift;
  $self->new_related( decodings => { name => shift, parameters => shift } );
}

sub merged_parameters {
  my ($self) = @_;

  use Hash::Merge::Simple qw( clone_merge );
  clone_merge
    $self->grammar->parameters,
    $self->lexicon->parameters,
    $self->model->parameters,
    $self->parameters;
}

around render => sub {
  my $orig = shift;
  my $self = shift;

  my $render = $self->$orig(
    qw( name type grammar lexicon model merged_parameters )
  );

  $render->{parameters} = delete $render->{merged_parameters};

  return $render;
};

sub namespace {
  my $app = shift->application;
  return join '/', $app->namespace, $app->name;
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding UTF-8

=head1 NAME

CloudCAST::Schema::Result::Recogniser - An abstract speech recogniser

=head1 DESCRIPTION

Objects of this class represent an abstract speech recogniser. They are
the result of combining
a language model (see L<CloudCAST::Scheme::Result::RecognitionModel>),
a grammar (see L<CloudCAST::Scheme::Result::Grammar>), and
a lexicon (see L<CloudCAST::Scheme::Result::Lexicon>).

These abstract recognisers cannot be directly used for decoding audio data.
For that, they need to be instantiated as a I<Decoding> (see
L<CloudCAST::Scheme::Result::Decoding>).

=head1 ACKNOWLEDGEMENTS

Work on the design of this table (and others in the schema) owes a considerable
amount to the input and feedback from the rest of the CloudCAST team, and in
particular:

=over 4

=item * Phil Green (University of Sheffield)

=item * Heidi Christensen (University of Sheffield)

=item * Andre Coy (University of West Indies)

=item * Frank Ludcicz (University of Toronto)

=item * Massimiliano Malavasi (Ausilioteca AIAS)

=item * Lorenzo Desideri (Ausilioteca AIAS)

=item * Ricard Marxer (University of Sheffield)

=item * Jochen Farwer (University of Sheffield)

=back

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
