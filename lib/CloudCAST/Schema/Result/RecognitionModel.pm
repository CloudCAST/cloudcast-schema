package CloudCAST::Schema::Result::RecognitionModel;

our $VERSION = '0';

use Moose;
use MooseX::MarkAsMethods autoclean => 1;

extends 'CloudCAST::Schema::Result';

__PACKAGE__->table('recognition_model');

__PACKAGE__->add_columns(
  id => {
    data_type => 'integer',
    size => 10,
    is_auto_increment => 1,
    extra => {
      unsigned => 1,
    },
  },

  name => {
    data_type => 'varchar',
    size => 100,
  },

  application => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  visibility => {
    data_type => 'integer',
    size => 10,
    is_nullable => 0,
    dynamic_default_on_create => sub { 0 },
    extra => {
      unsigned => 1,
    },
  },

  data => {
    data_type => 'varchar',
  },

  parameters => {
    data_type => 'json',
    serializer_class => 'JSON',
  },

  notes => {
    data_type => 'varchar',
    size => 1000,
  },

  date_created => {
    data_type => 'datetime',
    set_on_create => 1,
  },

  date_updated => {
    data_type => 'datetime',
    set_on_create => 1,
    set_on_update => 1,
  },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->add_unique_constraint([ qw( application name ) ]);

__PACKAGE__->has_many(
  recognisers => 'CloudCAST::Schema::Result::Recogniser', 'model',
#   { cascade_delete => 1 },
);

__PACKAGE__->belongs_to(
  application => 'CloudCAST::Schema::Result::Application', undef,
  { join_type => 'left' },
);

sub namespace {
  my $app = shift->application;
  return join '/', $app->namespace, $app->name;
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding UTF-8

=head1 NAME

CloudCAST::Schema::Result::RecognitionModel - A language model

=head1 DESCRIPTION

This class stores information about language models that can be used for
automatic speech reconigition. They store information about the data that they
were trained on, as well as the parameters that were used for their training.

This language model cannot I<directly> be used for speech recognition. Instead,
it can be used to define abstract recognisers
(see L<CloudCAST::Schema::Result::Recogniser>), which can in turn be
instantiated and used to decode audio data
(see L<CloudCAST::Schema::Result::Decoding>).

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut

=head1 ACKNOWLEDGEMENTS

Work on the design of this table (and others in the schema) owes a considerable
amount to the input and feedback from the rest of the CloudCAST team, and in
particular:

=over 4

=item * Phil Green (University of Sheffield)

=item * Heidi Christensen (University of Sheffield)

=item * Andre Coy (University of West Indies)

=item * Frank Ludcicz (University of Toronto)

=item * Massimiliano Malavasi (Ausilioteca AIAS)

=item * Lorenzo Desideri (Ausilioteca AIAS)

=item * Ricard Marxer (University of Sheffield)

=item * Jochen Farwer (University of Sheffield)

=back

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
