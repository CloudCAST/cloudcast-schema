package CloudCAST::Schema::Result::User;

our $VERSION = '0';

use Moose;
use DateTime;
use MooseX::MarkAsMethods autoclean => 1;

extends 'CloudCAST::Schema::Result';

__PACKAGE__->table('user_account');

__PACKAGE__->add_columns(
  id => {
    data_type => 'integer',
    size => 10,
    is_auto_increment => 1,
    extra => {
      unsigned => 1,
    },
  },

  name => {
    data_type => 'varchar',
    size => 64,
  },

  email_address => {
    data_type => 'varchar',
    size => 255,
  },

  first_name => {
    data_type => 'varchar',
    size => 255,
    is_nullable => 1,
  },

  last_name => {
    data_type => 'varchar',
    size => 255,
    is_nullable => 1,
  },

  is_admin => {
    data_type => 'boolean',
    default_value => 0,
  },

  password => {
    data_type => 'varchar',
    size => 200,
    is_nullable => 1,
  },

  password_salt => {
    data_type => 'varchar',
    size => 50,
    is_nullable => 1,
  },

  hash_method => {
    data_type => 'varchar',
    size => 50,
    is_nullable => 1,
  },

  date_created => {
    data_type => 'datetime',
    set_on_create => 1,
  },

  date_updated => {
    data_type => 'datetime',
    set_on_create => 1,
    set_on_update => 1,
  },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
  owned_applications => 'CloudCAST::Schema::Result::Application', 'owner',
#   { cascade_delete => 1 },
);

__PACKAGE__->has_many(
  user_keys => 'CloudCAST::Schema::Result::UserKey', 'owner',
  { cascade_delete => 1 },
);

__PACKAGE__->has_many(
  application_users => 'CloudCAST::Schema::Result::ApplicationUser', 'member',
);

sub keys {
  my ($self, $appid, $valid, $now) = @_;

  $valid //= 1;
  $now //= DateTime->now;

  # See DBIx/Class/Manual/FAQ.pod#..-format-a-DateTime-object-for-searching?
  my $dtf = $self->result_source->storage->datetime_parser;

  $now = $dtf->format_datetime( $now );

  my $cond = $valid
    ? [ {  '=', undef }, { '>'  => $now } ]
    : [ { '!=', undef }, { '<=' => $now } ];

  return $self->user_keys->search(
    {
      application => $appid,
      expiration_date => $cond,
    },
    {
      order_by => { -desc => 'date_created' },
    },
  );
}

# As per http://web.archive.org/web/20070204122440/dbix-class.shadowcatsystems.co.uk/index.cgi?ManyToManyWithAttributes
sub applications {
  my ($self, $appname) = @_;

  my $search = {};
  if ($appname) {
    my ($namespace, $name) = split qr{/}, $appname, 2;
    $search = { namespace => $namespace, name => $name };
  }

  return $self->application_users($search, { prefetch => ['application'] });
}

sub load_owned_application {
  my ($self, $search) = @_;
  $search = ref $search ? $search : { name => $search };
  return $self->owned_applications($search);
}

sub expire_keys {
  my ($self, $appid, $now) = @_;
  $now //= DateTime->now;
  $self->keys($appid)->update_all({ expiration_date => $now });
}

sub generate_key {
  my ($self, $appid, $now) = @_;
  $now //= DateTime->now;

  # Expire all keys are not yet expired
  $self->expire_keys($appid);

  return $self->create_related( user_keys => {
    application  => $appid,
    key          => $self->_random_key,
    date_created => $now,
  });
}

sub _random_key {
  my ($self) = @_;

  require Math::Random::Secure;
  require Encode::Base58;

  my $number = join '', map {
    sprintf('%010s', Math::Random::Secure::irand('9' x 11))
  } (1 .. 6);

  return substr(Encode::Base58::encode_base58( $number ), 0, 30);
}

sub display_name {
  my ($self) = @_;

  my $name  = $self->name;
  my $first = $self->first_name;
  my $last  = $self->last_name;

  return "$first $last" if $first and $last;
  return  $first        if $first;
  return  $last         if $last;
  return  $name;
}

around render => sub {
  my $orig = shift;
  my $self = shift;

  my $render = $self->$orig(qw( name email_address first_name last_name ));

  $render->{applications} = [
    map {
      {
        name => $_->application->name,
        role => $_->role->name,
      }
    } $self->applications
  ];

  return $render;
};

sub generate_password {
  my ($self) = @_;

  require Crypt::HSXKPasswd;
  return Crypt::HSXKPasswd->new(
    preset => 'SECURITYQ',
    preset_overrides => {
      padding_alphabet => [qw{! ?}],
      num_words => 5,
      case_transform => 'CAPITALISE',
      separator_character => q{ },
    },
  )->password;
}

sub validate_password {
  my ($self, $password, $cost) = @_;
  $cost //= 10;

  require MIME::Base64;
  require Authen::Passphrase::BlowfishCrypt;

  my $salted = Authen::Passphrase::BlowfishCrypt->new(
    salt => MIME::Base64::decode_base64( $self->password_salt ),
    cost => $cost,
    passphrase => $password,
  );

  return $salted->hash_base64 eq $self->password;
}

sub salt_password {
  my ($self, $password, $cost) = @_;
  $password //= $self->generate_password;
  $cost //= 10;

  require MIME::Base64;
  require Authen::Passphrase::BlowfishCrypt;

  my $salted = Authen::Passphrase::BlowfishCrypt->new(
    cost => $cost,
    salt_random => 1,
    passphrase => $password,
  );

  return {
    hash => $salted->hash_base64,
    salt => MIME::Base64::encode_base64( $salted->salt ),
  };
}

__PACKAGE__->add_unique_constraint([ qw( name ) ]);

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding UTF-8

=head1 NAME

CloudCAST::Schema::Result::User - A CloudCAST user

=head1 DESCRIPTION

Objects of this class represent CloudCAST user accounts. These may be linked to
an actually existing person, or they may be created only to specify a namespace
under which applications may exist (see
L<CloudCAST::Schema::Result::Applications>).

=head1 ACKNOWLEDGEMENTS

Work on the design of this table (and others in the schema) owes a considerable
amount to the input and feedback from the rest of the CloudCAST team, and in
particular:

=over 4

=item * Phil Green (University of Sheffield)

=item * Heidi Christensen (University of Sheffield)

=item * Andre Coy (University of West Indies)

=item * Frank Ludcicz (University of Toronto)

=item * Massimiliano Malavasi (Ausilioteca AIAS)

=item * Lorenzo Desideri (Ausilioteca AIAS)

=item * Ricard Marxer (University of Sheffield)

=item * Jochen Farwer (University of Sheffield)

=back

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
