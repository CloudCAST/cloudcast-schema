package CloudCAST::Schema::Result::UserKey;

our $VERSION = '0';

use Moose;
use MooseX::MarkAsMethods autoclean => 1;

extends 'CloudCAST::Schema::Result';

__PACKAGE__->table('user_key');

__PACKAGE__->add_columns(
  id => {
    data_type => 'integer',
    size => 10,
    is_auto_increment => 1,
    extra => {
      unsigned => 1,
    },
  },

  owner => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
  },

  application => {
    data_type => 'integer',
    is_numeric => 1,
    is_foreign_key => 1,
    extra => {
      unsigned => 1,
    },
    is_nullable => 1,
  },

  key => {
    data_type => 'char',
    size => 30,
  },

  date_created => {
    data_type => 'datetime',
    set_on_create => 1,
  },

  expiration_date => {
    data_type => 'datetime',
    is_nullable => 1,
  },

);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
  owner => 'CloudCAST::Schema::Result::User', undef,
  { proxy => [qw(
    id name email_address first_name last_name
    password password_salt hash_method
    date_created date_updated
  )] },
);

__PACKAGE__->belongs_to(
  application => 'CloudCAST::Schema::Result::Application', undef,
  { join_type => 'left' },
);

sub render {
  my ($self) = @_;
  my $render = {
    map { $_ => $self->$_ } qw( key application expiration_date )
  };

  foreach (keys %{$render}) {
    delete $render->{$_} unless $render->{$_};
  }

  $render->{application} = $self->application->uri_name
    if $render->{application};

  return $render;
}

__PACKAGE__->meta->make_immutable;

1;

=encoding UTF-8

=head1 NAME

CloudCAST::Schema::Result::UserKey - An API key for a specific user

=head1 DESCRIPTION

This class allows a CloudCAST server to store (encrypted) copies of the API
keys that are linked to a specific user account, either to be used globally
(for actions on that user and all applications they may have access to), or
for limited use, only on a specific application under which taht user has been
granted some role (see L<CloudCAST::Schema::Result::ApplicationUser>).

Any one user I<should> only have one global user key active at any given time.

=head1 ACKNOWLEDGEMENTS

Work on the design of this table (and others in the schema) owes a considerable
amount to the input and feedback from the rest of the CloudCAST team, and in
particular:

=over 4

=item * Phil Green (University of Sheffield)

=item * Heidi Christensen (University of Sheffield)

=item * Andre Coy (University of West Indies)

=item * Frank Ludcicz (University of Toronto)

=item * Massimiliano Malavasi (Ausilioteca AIAS)

=item * Lorenzo Desideri (Ausilioteca AIAS)

=item * Ricard Marxer (University of Sheffield)

=item * Jochen Farwer (University of Sheffield)

=back

=head1 AUTHOR

=over 4

=item * José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by University of Sheffield.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
