package CloudCAST::Schema::ResultSet;

our $VERSION = '0';

use Moose;
use MooseX::MarkAsMethods autoclean => 1;
use MooseX::NonMoose;

extends 'DBIx::Class::ResultSet';

__PACKAGE__->meta->make_immutable;

1;
