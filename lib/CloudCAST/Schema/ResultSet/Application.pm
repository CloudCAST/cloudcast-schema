package CloudCAST::Schema::ResultSet::Application;

our $VERSION = '0';

use uni::perl;
use Moose;
use MooseX::MarkAsMethods autoclean => 1;
use String::CamelSnakeKebab qw( kebab_case );

extends 'CloudCAST::Schema::ResultSet';

around new_result => sub {
  my $orig = shift;
  my $self = shift;
  my $new  = shift;

  $new->{application_users} = [ { member => $new->{owner}, role => 'owner' } ];

  return $self->$orig($new);
};

__PACKAGE__->meta->make_immutable;

1;
