package CloudCAST::Schema::ResultSet::User;

our $VERSION = '0';

use uni::perl;
use Moose;
use CloudCAST::Schema::Result::User;
use MooseX::MarkAsMethods autoclean => 1;

extends 'CloudCAST::Schema::ResultSet';

around new_result => sub {
  my $orig = shift;
  my $self = shift;
  my $new  = shift;

  unless ($new->{password} and $new->{password_salt}) {

    $new->{password} //= CloudCAST::Schema::Result::User->generate_password;
    my $salted = CloudCAST::Schema::Result::User->salt_password($new->{password});

    $new->{hash_method}   = 'bcrypt';
    $new->{password}      = $salted->{hash};
    $new->{password_salt} = $salted->{salt};
  }

  return $self->$orig($new);
};

__PACKAGE__->meta->make_immutable;

1;
