-- 
-- Created by SQL::Translator::Producer::PostgreSQL
-- Created on Fri Dec  1 15:49:45 2017
-- 
;
--
-- Table: grammar
--
CREATE TABLE "grammar" (
  "id" serial NOT NULL,
  "data" json NOT NULL,
  "notes" text NOT NULL,
  "date_created" timestamp NOT NULL,
  "date_updated" timestamp NOT NULL,
  PRIMARY KEY ("id")
);

;
--
-- Table: lexicon
--
CREATE TABLE "lexicon" (
  "id" serial NOT NULL,
  "data" json NOT NULL,
  "notes" character varying(1000) NOT NULL,
  "date_created" timestamp NOT NULL,
  "date_updated" timestamp NOT NULL,
  PRIMARY KEY ("id")
);

;
--
-- Table: meta_access_level
--
CREATE TABLE "meta_access_level" (
  "name" character varying(10) NOT NULL,
  "description" text NOT NULL,
  "can_read_phi" boolean DEFAULT '0' NOT NULL,
  "can_train_model" boolean DEFAULT '0' NOT NULL,
  "can_upload" boolean DEFAULT '0' NOT NULL,
  "can_download" boolean DEFAULT '0' NOT NULL,
  "can_write_owner" boolean DEFAULT '0' NOT NULL,
  "can_write_manager" boolean DEFAULT '0' NOT NULL,
  "can_write_user" boolean DEFAULT '0' NOT NULL,
  "can_write_app" boolean DEFAULT '0' NOT NULL,
  "date_created" timestamp NOT NULL,
  "date_updated" timestamp NOT NULL,
  PRIMARY KEY ("name")
);

;
--
-- Table: meta_country
--
CREATE TABLE "meta_country" (
  "id" character varying(2) NOT NULL,
  "name" character varying(255) NOT NULL,
  PRIMARY KEY ("id")
);

;
--
-- Table: meta_data_type
--
CREATE TABLE "meta_data_type" (
  "id" character(3) NOT NULL,
  "name" character varying(100) NOT NULL,
  "is_text" boolean NOT NULL,
  "is_audio" boolean NOT NULL,
  "is_video" boolean NOT NULL,
  PRIMARY KEY ("id")
);

;
--
-- Table: meta_gender
--
CREATE TABLE "meta_gender" (
  "id" character(1) NOT NULL,
  "name" character varying(20) NOT NULL,
  PRIMARY KEY ("id")
);

;
--
-- Table: meta_language
--
CREATE TABLE "meta_language" (
  "id" character varying(2) NOT NULL,
  "name" character varying(60) NOT NULL,
  PRIMARY KEY ("id")
);

;
--
-- Table: meta_location
--
CREATE TABLE "meta_location" (
  "id" serial NOT NULL,
  "name" character varying(20) NOT NULL,
  PRIMARY KEY ("id")
);

;
--
-- Table: recognition_model
--
CREATE TABLE "recognition_model" (
  "id" serial NOT NULL,
  "name" character varying(100) NOT NULL,
  "notes" character varying(1000) NOT NULL,
  "parameters" json NOT NULL,
  "date_created" timestamp NOT NULL,
  "date_updated" timestamp NOT NULL,
  PRIMARY KEY ("id")
);

;
--
-- Table: collection
--
CREATE TABLE "collection" (
  "id" serial NOT NULL,
  "name" character varying(200) NOT NULL,
  "institution" character varying(200),
  "city" character varying(255),
  "country" character varying(2),
  "license" character varying(200),
  "contact_name" character varying(100),
  "contact_email" character varying(100),
  "contact_phone" character varying(30),
  "contact_address" character varying(100),
  "date_created" timestamp NOT NULL,
  "date_updated" timestamp NOT NULL,
  PRIMARY KEY ("id")
);
CREATE INDEX "collection_idx_country" on "collection" ("country");

;
--
-- Table: user_account
--
CREATE TABLE "user_account" (
  "id" serial NOT NULL,
  "name" character varying(64) NOT NULL,
  "email_address" character varying(255) NOT NULL,
  "first_name" character varying(255),
  "last_name" character varying(255),
  "permissions" character varying NOT NULL,
  "password" character varying(200),
  "password_salt" character varying(50),
  "hash_method" character varying(50),
  "date_created" timestamp NOT NULL,
  "date_updated" timestamp NOT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "user_account_name" UNIQUE ("name")
);
CREATE INDEX "user_account_idx_permissions" on "user_account" ("permissions");

;
--
-- Table: grammar_lexicon
--
CREATE TABLE "grammar_lexicon" (
  "grammar" integer NOT NULL,
  "lexicon" integer NOT NULL
);
CREATE INDEX "grammar_lexicon_idx_grammar" on "grammar_lexicon" ("grammar");
CREATE INDEX "grammar_lexicon_idx_lexicon" on "grammar_lexicon" ("lexicon");

;
--
-- Table: application
--
CREATE TABLE "application" (
  "id" serial NOT NULL,
  "parent" integer,
  "owner" integer NOT NULL,
  "namespace" character varying(64) NOT NULL,
  "name" character varying(255),
  "display_name" character varying(255),
  "description" character varying(255),
  "visibility" integer NOT NULL,
  "default_decoding" integer,
  "date_created" timestamp NOT NULL,
  "date_updated" timestamp NOT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "application_owner_name" UNIQUE ("owner", "name")
);
CREATE INDEX "application_idx_default_decoding" on "application" ("default_decoding");
CREATE INDEX "application_idx_owner" on "application" ("owner");
CREATE INDEX "application_idx_parent" on "application" ("parent");

;
--
-- Table: collection_session
--
CREATE TABLE "collection_session" (
  "id" serial NOT NULL,
  "collection" integer NOT NULL,
  "date_started" timestamp NOT NULL,
  "date_ended" timestamp,
  "equipment" character varying(500) NOT NULL,
  "location" integer NOT NULL,
  "country" character varying(2) NOT NULL,
  "city" character varying(255) NOT NULL,
  "date_created" timestamp NOT NULL,
  "date_updated" timestamp NOT NULL,
  PRIMARY KEY ("id")
);
CREATE INDEX "collection_session_idx_collection" on "collection_session" ("collection");
CREATE INDEX "collection_session_idx_country" on "collection_session" ("country");
CREATE INDEX "collection_session_idx_location" on "collection_session" ("location");

;
--
-- Table: recogniser
--
CREATE TABLE "recogniser" (
  "id" serial NOT NULL,
  "name" character varying(100) NOT NULL,
  "type" character varying(100) NOT NULL,
  "grammar" integer NOT NULL,
  "lexicon" integer NOT NULL,
  "model" integer NOT NULL,
  "parameters" json NOT NULL,
  "training_scheduled" boolean NOT NULL,
  "date_created" timestamp NOT NULL,
  "date_updated" timestamp NOT NULL,
  PRIMARY KEY ("id")
);
CREATE INDEX "recogniser_idx_grammar" on "recogniser" ("grammar");
CREATE INDEX "recogniser_idx_lexicon" on "recogniser" ("lexicon");
CREATE INDEX "recogniser_idx_model" on "recogniser" ("model");

;
--
-- Table: application_user
--
CREATE TABLE "application_user" (
  "member" integer NOT NULL,
  "application" integer NOT NULL,
  "role" character varying(10) NOT NULL
);
CREATE INDEX "application_user_idx_application" on "application_user" ("application");
CREATE INDEX "application_user_idx_member" on "application_user" ("member");
CREATE INDEX "application_user_idx_role" on "application_user" ("role");

;
--
-- Table: collection_participant
--
CREATE TABLE "collection_participant" (
  "id" serial NOT NULL,
  "collection" integer NOT NULL,
  "first_name" character varying(50),
  "last_name" character varying(50),
  "consent_substitute" boolean DEFAULT '0' NOT NULL,
  "consent_substitute_name" character varying(100) DEFAULT '' NOT NULL,
  "gender" character(1) NOT NULL,
  "age_year" smallint,
  "age_month" smallint,
  "dob" timestamp,
  "dob_month" smallint,
  "dob_year" smallint,
  "education_year" smallint NOT NULL,
  "native_language" character varying(2) NOT NULL,
  "contact_email" character varying(100),
  "contact_phone" character varying(30),
  "contact_address" character varying(100),
  "diagnosis" character varying(255),
  "diagnosis_code" character varying(50),
  "score_mmse" smallint,
  "score_moca" smallint,
  "score_updrs" smallint,
  "score_hrsd" smallint,
  "score_gsd" smallint,
  "date_created" timestamp NOT NULL,
  "date_updated" timestamp NOT NULL,
  PRIMARY KEY ("id")
);
CREATE INDEX "collection_participant_idx_collection" on "collection_participant" ("collection");
CREATE INDEX "collection_participant_idx_gender" on "collection_participant" ("gender");
CREATE INDEX "collection_participant_idx_native_language" on "collection_participant" ("native_language");

;
--
-- Table: user_key
--
CREATE TABLE "user_key" (
  "id" serial NOT NULL,
  "owner" integer NOT NULL,
  "application" integer,
  "key" character(30) NOT NULL,
  "date_created" timestamp NOT NULL,
  "expiration_date" timestamp,
  PRIMARY KEY ("id")
);
CREATE INDEX "user_key_idx_application" on "user_key" ("application");
CREATE INDEX "user_key_idx_owner" on "user_key" ("owner");

;
--
-- Table: decoding
--
CREATE TABLE "decoding" (
  "id" serial NOT NULL,
  "name" character varying(100),
  "recogniser" integer NOT NULL,
  "application" integer NOT NULL,
  "parameters" json NOT NULL,
  "date_created" timestamp NOT NULL,
  "date_updated" timestamp NOT NULL,
  PRIMARY KEY ("id")
);
CREATE INDEX "decoding_idx_application" on "decoding" ("application");
CREATE INDEX "decoding_idx_recogniser" on "decoding" ("recogniser");

;
--
-- Table: decoding_result
--
CREATE TABLE "decoding_result" (
  "input" character varying(100) NOT NULL,
  "input_type" character varying(100) NOT NULL,
  "output" json NOT NULL,
  "parameters" integer NOT NULL,
  "date_created" timestamp NOT NULL
);
CREATE INDEX "decoding_result_idx_parameters" on "decoding_result" ("parameters");

;
--
-- Table: collection_session_item
--
CREATE TABLE "collection_session_item" (
  "id" serial NOT NULL,
  "collection" integer NOT NULL,
  "session" integer NOT NULL,
  "participant" integer NOT NULL,
  "date_started" timestamp NOT NULL,
  "date_ended" timestamp,
  "type" character(3) NOT NULL,
  "data" text NOT NULL,
  "date_created" timestamp NOT NULL,
  "date_updated" timestamp NOT NULL,
  PRIMARY KEY ("id")
);
CREATE INDEX "collection_session_item_idx_collection" on "collection_session_item" ("collection");
CREATE INDEX "collection_session_item_idx_participant" on "collection_session_item" ("participant");
CREATE INDEX "collection_session_item_idx_session" on "collection_session_item" ("session");
CREATE INDEX "collection_session_item_idx_type" on "collection_session_item" ("type");

;
--
-- Foreign Key Definitions
--

;
ALTER TABLE "collection" ADD CONSTRAINT "collection_fk_country" FOREIGN KEY ("country")
  REFERENCES "meta_country" ("id") DEFERRABLE;

;
ALTER TABLE "user_account" ADD CONSTRAINT "user_account_fk_permissions" FOREIGN KEY ("permissions")
  REFERENCES "meta_access_level" ("name") DEFERRABLE;

;
ALTER TABLE "grammar_lexicon" ADD CONSTRAINT "grammar_lexicon_fk_grammar" FOREIGN KEY ("grammar")
  REFERENCES "grammar" ("id") DEFERRABLE;

;
ALTER TABLE "grammar_lexicon" ADD CONSTRAINT "grammar_lexicon_fk_lexicon" FOREIGN KEY ("lexicon")
  REFERENCES "lexicon" ("id") DEFERRABLE;

;
ALTER TABLE "application" ADD CONSTRAINT "application_fk_default_decoding" FOREIGN KEY ("default_decoding")
  REFERENCES "decoding" ("id") DEFERRABLE;

;
ALTER TABLE "application" ADD CONSTRAINT "application_fk_owner" FOREIGN KEY ("owner")
  REFERENCES "user_account" ("id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE "application" ADD CONSTRAINT "application_fk_parent" FOREIGN KEY ("parent")
  REFERENCES "application" ("id") DEFERRABLE;

;
ALTER TABLE "collection_session" ADD CONSTRAINT "collection_session_fk_collection" FOREIGN KEY ("collection")
  REFERENCES "collection" ("id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE "collection_session" ADD CONSTRAINT "collection_session_fk_country" FOREIGN KEY ("country")
  REFERENCES "meta_country" ("id") DEFERRABLE;

;
ALTER TABLE "collection_session" ADD CONSTRAINT "collection_session_fk_location" FOREIGN KEY ("location")
  REFERENCES "meta_location" ("id") DEFERRABLE;

;
ALTER TABLE "recogniser" ADD CONSTRAINT "recogniser_fk_grammar" FOREIGN KEY ("grammar")
  REFERENCES "grammar" ("id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE "recogniser" ADD CONSTRAINT "recogniser_fk_lexicon" FOREIGN KEY ("lexicon")
  REFERENCES "lexicon" ("id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE "recogniser" ADD CONSTRAINT "recogniser_fk_model" FOREIGN KEY ("model")
  REFERENCES "recognition_model" ("id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE "application_user" ADD CONSTRAINT "application_user_fk_application" FOREIGN KEY ("application")
  REFERENCES "application" ("id") ON DELETE CASCADE DEFERRABLE;

;
ALTER TABLE "application_user" ADD CONSTRAINT "application_user_fk_member" FOREIGN KEY ("member")
  REFERENCES "user_account" ("id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE "application_user" ADD CONSTRAINT "application_user_fk_role" FOREIGN KEY ("role")
  REFERENCES "meta_access_level" ("name") DEFERRABLE;

;
ALTER TABLE "collection_participant" ADD CONSTRAINT "collection_participant_fk_collection" FOREIGN KEY ("collection")
  REFERENCES "collection" ("id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE "collection_participant" ADD CONSTRAINT "collection_participant_fk_gender" FOREIGN KEY ("gender")
  REFERENCES "meta_gender" ("id") DEFERRABLE;

;
ALTER TABLE "collection_participant" ADD CONSTRAINT "collection_participant_fk_native_language" FOREIGN KEY ("native_language")
  REFERENCES "meta_language" ("id") DEFERRABLE;

;
ALTER TABLE "user_key" ADD CONSTRAINT "user_key_fk_application" FOREIGN KEY ("application")
  REFERENCES "application" ("id") DEFERRABLE;

;
ALTER TABLE "user_key" ADD CONSTRAINT "user_key_fk_owner" FOREIGN KEY ("owner")
  REFERENCES "user_account" ("id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE "decoding" ADD CONSTRAINT "decoding_fk_application" FOREIGN KEY ("application")
  REFERENCES "application" ("id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE "decoding" ADD CONSTRAINT "decoding_fk_recogniser" FOREIGN KEY ("recogniser")
  REFERENCES "recogniser" ("id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE "decoding_result" ADD CONSTRAINT "decoding_result_fk_parameters" FOREIGN KEY ("parameters")
  REFERENCES "decoding" ("id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE "collection_session_item" ADD CONSTRAINT "collection_session_item_fk_collection" FOREIGN KEY ("collection")
  REFERENCES "collection" ("id") DEFERRABLE;

;
ALTER TABLE "collection_session_item" ADD CONSTRAINT "collection_session_item_fk_participant" FOREIGN KEY ("participant")
  REFERENCES "collection_participant" ("id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE "collection_session_item" ADD CONSTRAINT "collection_session_item_fk_session" FOREIGN KEY ("session")
  REFERENCES "collection_session" ("id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE "collection_session_item" ADD CONSTRAINT "collection_session_item_fk_type" FOREIGN KEY ("type")
  REFERENCES "meta_data_type" ("id") DEFERRABLE;

;
