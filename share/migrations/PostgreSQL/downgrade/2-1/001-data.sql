-- Convert schema '/home/baboso/work/proj/cloudcast/devel/schema/share/migrations/_source/deploy/2/001-auto.yml' to '/home/baboso/work/proj/cloudcast/devel/schema/share/migrations/_source/deploy/1/001-auto.yml':;

;
BEGIN;

;
ALTER TABLE decoding_result ADD COLUMN parameters integer;

;
CREATE INDEX decoding_result_idx_parameters on decoding_result (parameters);

;
ALTER TABLE decoding_result ADD CONSTRAINT decoding_result_fk_parameters FOREIGN KEY (parameters)
  REFERENCES decoding (id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;

COMMIT;

