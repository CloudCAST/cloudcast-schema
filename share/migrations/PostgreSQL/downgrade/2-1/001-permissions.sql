-- Convert schema '/home/baboso/work/proj/cloudcast/devel/schema/share/migrations/_source/deploy/2/001-auto.yml' to '/home/baboso/work/proj/cloudcast/devel/schema/share/migrations/_source/deploy/1/001-auto.yml':;

;
BEGIN;

;
ALTER TABLE meta_access_level ADD COLUMN can_read_phi boolean DEFAULT '0';

;
ALTER TABLE meta_access_level ADD COLUMN can_write_app boolean DEFAULT '0';

;
ALTER TABLE user_account ADD COLUMN permissions character varying;

;
CREATE INDEX user_account_idx_permissions on user_account (permissions);

;
ALTER TABLE user_account ADD CONSTRAINT user_account_fk_permissions FOREIGN KEY (permissions)
  REFERENCES meta_access_level (name) DEFERRABLE;

;

COMMIT;

