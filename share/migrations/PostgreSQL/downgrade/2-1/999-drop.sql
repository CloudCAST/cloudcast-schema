-- Convert schema '/home/baboso/work/proj/cloudcast/devel/schema/share/migrations/_source/deploy/2/001-auto.yml' to '/home/baboso/work/proj/cloudcast/devel/schema/share/migrations/_source/deploy/1/001-auto.yml':;

;
BEGIN;

;
ALTER TABLE application_user DROP CONSTRAINT application_user_fk_recogniser;

;
DROP INDEX application_user_idx_recogniser;

;
ALTER TABLE application_user DROP COLUMN recogniser;

;
ALTER TABLE decoding DROP CONSTRAINT decoding_application_name;

;
ALTER TABLE decoding_result DROP CONSTRAINT decoding_result_fk_decoding;

;
DROP INDEX decoding_result_idx_decoding;

;
ALTER TABLE decoding_result DROP COLUMN decoding;

;
ALTER TABLE grammar DROP CONSTRAINT grammar_application_name;

;
ALTER TABLE grammar DROP CONSTRAINT grammar_fk_application;

;
DROP INDEX grammar_idx_application;

;
ALTER TABLE grammar DROP COLUMN name;

;
ALTER TABLE grammar DROP COLUMN application;

;
ALTER TABLE grammar DROP COLUMN visibility;

;
ALTER TABLE lexicon DROP CONSTRAINT lexicon_application_name;

;
ALTER TABLE lexicon DROP CONSTRAINT lexicon_fk_application;

;
DROP INDEX lexicon_idx_application;

;
ALTER TABLE lexicon DROP COLUMN name;

;
ALTER TABLE lexicon DROP COLUMN application;

;
ALTER TABLE lexicon DROP COLUMN visibility;

;
ALTER TABLE meta_access_level DROP COLUMN is_admin;

;
ALTER TABLE recogniser DROP CONSTRAINT recogniser_application_name;

;
ALTER TABLE recogniser DROP CONSTRAINT recogniser_fk_application;

;
DROP INDEX recogniser_idx_application;

;
ALTER TABLE recogniser DROP COLUMN application;

;
ALTER TABLE recogniser DROP COLUMN visibility;

;
ALTER TABLE recogniser DROP COLUMN data_set;

;
ALTER TABLE recogniser DROP COLUMN date_trained;

;
ALTER TABLE recognition_model DROP CONSTRAINT recognition_model_application_name;

;
ALTER TABLE recognition_model DROP CONSTRAINT recognition_model_fk_application;

;
DROP INDEX recognition_model_idx_application;

;
ALTER TABLE recognition_model DROP COLUMN application;

;
ALTER TABLE recognition_model DROP COLUMN visibility;

;
ALTER TABLE recognition_model DROP COLUMN data;

;
ALTER TABLE user_account DROP COLUMN is_admin;

;

COMMIT;

