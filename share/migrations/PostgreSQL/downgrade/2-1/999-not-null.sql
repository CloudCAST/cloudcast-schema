-- Convert schema '/home/baboso/work/proj/cloudcast/devel/schema/share/migrations/_source/deploy/2/001-auto.yml' to '/home/baboso/work/proj/cloudcast/devel/schema/share/migrations/_source/deploy/1/001-auto.yml':;

;
BEGIN;

;
ALTER TABLE application ALTER COLUMN namespace SET NOT NULL;

;
ALTER TABLE decoding_result ALTER COLUMN parameters SET NOT NULL;

;
ALTER TABLE meta_access_level ALTER COLUMN can_read_phi SET NOT NULL;

;
ALTER TABLE meta_access_level ALTER COLUMN can_write_app SET NOT NULL;

;
ALTER TABLE user_account ALTER COLUMN permissions SET NOT NULL;

;

COMMIT;

