-- Convert schema '/home/baboso/work/proj/cloudcast/devel/schema/share/migrations/_source/deploy/1/001-auto.yml' to '/home/baboso/work/proj/cloudcast/devel/schema/share/migrations/_source/deploy/2/001-auto.yml':;

;
BEGIN;

;
ALTER TABLE recogniser ADD COLUMN data_set json;

;
ALTER TABLE recogniser ADD COLUMN date_trained timestamp;

;
ALTER TABLE application_user ADD COLUMN recogniser integer;

;
CREATE INDEX application_user_idx_recogniser on application_user (recogniser);

;
ALTER TABLE application_user ADD CONSTRAINT application_user_fk_recogniser FOREIGN KEY (recogniser)
  REFERENCES recogniser (id) DEFERRABLE;

;
ALTER TABLE decoding ADD CONSTRAINT decoding_application_name UNIQUE (application, name);

;
ALTER TABLE decoding_result ADD COLUMN decoding integer;

;
CREATE INDEX decoding_result_idx_decoding on decoding_result (decoding);

;
ALTER TABLE decoding_result ADD CONSTRAINT decoding_result_fk_decoding FOREIGN KEY (decoding)
  REFERENCES decoding (id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE recognition_model ADD COLUMN data character varying;

;

COMMIT;

