-- Convert schema '/home/baboso/work/proj/cloudcast/devel/schema/share/migrations/_source/deploy/1/001-auto.yml' to '/home/baboso/work/proj/cloudcast/devel/schema/share/migrations/_source/deploy/2/001-auto.yml':;

;
BEGIN;

;
ALTER TABLE grammar ADD COLUMN name character varying(100);

;
ALTER TABLE grammar ADD COLUMN application integer;

;
ALTER TABLE grammar ADD COLUMN visibility integer;

;
CREATE INDEX grammar_idx_application on grammar (application);

;
ALTER TABLE grammar ADD CONSTRAINT grammar_application_name UNIQUE (application, name);

;
ALTER TABLE grammar ADD CONSTRAINT grammar_fk_application FOREIGN KEY (application)
  REFERENCES application (id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE lexicon ADD COLUMN name character varying(100);

;
ALTER TABLE lexicon ADD COLUMN application integer;

;
ALTER TABLE lexicon ADD COLUMN visibility integer;

;
CREATE INDEX lexicon_idx_application on lexicon (application);

;
ALTER TABLE lexicon ADD CONSTRAINT lexicon_application_name UNIQUE (application, name);

;
ALTER TABLE lexicon ADD CONSTRAINT lexicon_fk_application FOREIGN KEY (application)
  REFERENCES application (id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE recogniser ADD COLUMN application integer;

;
ALTER TABLE recogniser ADD COLUMN visibility integer;

;
CREATE INDEX recogniser_idx_application on recogniser (application);

;
ALTER TABLE recogniser ADD CONSTRAINT recogniser_application_name UNIQUE (application, name);

;
ALTER TABLE recogniser ADD CONSTRAINT recogniser_fk_application FOREIGN KEY (application)
  REFERENCES application (id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;
ALTER TABLE recognition_model ADD COLUMN application integer;

;
ALTER TABLE recognition_model ADD COLUMN visibility integer;

;
CREATE INDEX recognition_model_idx_application on recognition_model (application);

;
ALTER TABLE recognition_model ADD CONSTRAINT recognition_model_application_name UNIQUE (application, name);

;
ALTER TABLE recognition_model ADD CONSTRAINT recognition_model_fk_application FOREIGN KEY (application)
  REFERENCES application (id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE;

;

COMMIT;
