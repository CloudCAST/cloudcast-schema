-- Convert schema '/home/baboso/work/proj/cloudcast/devel/schema/share/migrations/_source/deploy/1/001-auto.yml' to '/home/baboso/work/proj/cloudcast/devel/schema/share/migrations/_source/deploy/2/001-auto.yml':;

;
BEGIN;

;
ALTER TABLE application DROP COLUMN namespace;

;
ALTER TABLE decoding_result DROP CONSTRAINT decoding_result_fk_parameters;

;
DROP INDEX decoding_result_idx_parameters;

;
ALTER TABLE decoding_result DROP COLUMN parameters;

;
ALTER TABLE meta_access_level DROP COLUMN can_read_phi;

;
ALTER TABLE meta_access_level DROP COLUMN can_write_app;

;
ALTER TABLE user_account DROP CONSTRAINT user_account_fk_permissions;

;
DROP INDEX user_account_idx_permissions;

;
ALTER TABLE user_account DROP COLUMN permissions;

;

COMMIT;

