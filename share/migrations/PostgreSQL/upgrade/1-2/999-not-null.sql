-- Convert schema '/home/baboso/work/proj/cloudcast/devel/schema/share/migrations/_source/deploy/1/001-auto.yml' to '/home/baboso/work/proj/cloudcast/devel/schema/share/migrations/_source/deploy/2/001-auto.yml':;

;
BEGIN;

;
ALTER TABLE meta_access_level ALTER COLUMN is_admin SET NOT NULL;

;
ALTER TABLE recogniser ALTER COLUMN application SET NOT NULL;

;
ALTER TABLE recogniser ALTER COLUMN visibility SET NOT NULL;

;
ALTER TABLE recogniser ALTER COLUMN data_set SET NOT NULL;

;
ALTER TABLE recognition_model ALTER COLUMN application SET NOT NULL;

;
ALTER TABLE recognition_model ALTER COLUMN visibility SET NOT NULL;

;
ALTER TABLE recognition_model ALTER COLUMN data SET NOT NULL;

;
ALTER TABLE user_account ALTER COLUMN is_admin SET NOT NULL;

;
ALTER TABLE decoding_result ALTER COLUMN decoding SET NOT NULL;

;
ALTER TABLE grammar ALTER COLUMN name SET NOT NULL;

;
ALTER TABLE grammar ALTER COLUMN application SET NOT NULL;

;
ALTER TABLE grammar ALTER COLUMN visibility SET NOT NULL;

;
ALTER TABLE lexicon ALTER COLUMN name SET NOT NULL;

;
ALTER TABLE lexicon ALTER COLUMN application SET NOT NULL;

;
ALTER TABLE lexicon ALTER COLUMN visibility SET NOT NULL;

;

COMMIT;

