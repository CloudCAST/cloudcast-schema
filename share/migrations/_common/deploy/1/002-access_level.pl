use strict;
use warnings;

use DBIx::Class::Migration::RunScript;

migrate {
  shift->schema
    ->resultset('MetaAccessLevel')
    ->populate([
      [qw(
        name
        description
        can_read_phi
        can_train_model
        can_upload
        can_download
        can_write_owner
        can_write_manager
        can_write_user
        can_write_app
        date_created
        date_updated
      )],
      [
        'admin',
        'A site admin',
        1, 1, 1, 1, 1, 1, 1, 1,
        DateTime->now,
        DateTime->now,
      ],
      [
        'none',
        'An empty set of permissions',
        0, 0, 0, 0, 0, 0, 0, 0,
        DateTime->now,
        DateTime->now,
      ],
      [
        'owner',
        'The owner of an application',
        1, 1, 1, 1, 0, 1, 1, 0,
        DateTime->now,
        DateTime->now,
      ],
      [
        'manager',
        'The manager of an application',
        1, 0, 1, 1, 0, 0, 1, 0,
        DateTime->now,
        DateTime->now,
      ],
      [
        'user',
        'A user of an application',
        1, 0, 1, 0, 0, 0, 0, 0,
        DateTime->now,
        DateTime->now,
      ],
    ]);
};
