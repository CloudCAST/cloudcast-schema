use strict;
use warnings;

use DBIx::Class::Migration::RunScript;
use Encode qw( encode_utf8 decode_utf8 );

migrate {
  shift->schema->resultset('MetaDataType')
    ->populate([
      [qw( id name is_audio is_video is_text )],
      ['a--' => 'Audio only',            1, 0, 0],
      ['-v-' => 'Video only',            0, 1, 0],
      ['--t' => 'Text',                  0, 0, 1],
      ['av-' => 'Audiovisual',           1, 1, 0],
      ['a-t' => 'Audio with text',       1, 0, 1],
      ['-vt' => 'Video with text',       0, 1, 1],
      ['avt' => 'Audiovisual with text', 1, 1, 1],
    ]);
};
