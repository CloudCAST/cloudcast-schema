use strict;
use warnings;

use DBIx::Class::Migration::RunScript;
use Encode qw( encode_utf8 decode_utf8 );

migrate {
  my $rs = shift->schema->resultset('MetaGender')
    ->populate([
      [qw( id name )],
      [ f => 'Female'],
      [ m => 'Male' ],
    ]);
};
