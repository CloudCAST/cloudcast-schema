use strict;
use warnings;

use DBIx::Class::Migration::RunScript;
use Encode qw( encode_utf8 decode_utf8 );

migrate {
  shift->schema->resultset('MetaLocation')
    ->populate([
      [qw( id name  )],
      [ 1 => 'Home' ],
      [ 2 => 'Lab'  ],
    ]);
};
