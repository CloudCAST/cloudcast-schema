use strict;
use warnings;

use DBIx::Class::Migration::RunScript;
use DateTime;

migrate {
  my $db = shift;

  my $rs = $db->schema->resultset('Collection');
  my $collection = $rs->create({
    name            => 'TORGO',
    institution     => 'ABC',
    city            => 'Glasgow',
    country         => 'gb',
    license         => 'GPL',
    contact_name    => 'Mario Maracus',
    contact_email   => 'mister@tea.co.uk',
    contact_address => 'Carlton Close, W1 3CS, London',
    contact_phone   => '1234567890',
    date_created    => DateTime->now,
    date_updated    => DateTime->now,
    collection_participants => [
      {
        first_name      => 'Mary',
        last_name       => 'Piper',
        gender          => 'f',
        age_year        => 23,
        age_month       => 279,
        dob             => '16/12/1993/',
        dob_month       => 12,
        dob_year        => 1993,
        education_year  => 17,
        native_language => 'en',
        contact_email   => 'mary@piper.uk',
        contact_address => 'Test Rd., S3 4DE, New South Norfolk',
        contact_phone   => '555 555 555',
        date_created    => DateTime->now,
        date_updated    => DateTime->now,
      },
      {
        first_name      => 'Peter',
        last_name       => 'Pepper',
        gender          => 'm',
        age_year        => 32,
        age_month       => 387,
        dob             => '16/12/1983/',
        dob_month       => 12,
        dob_year        => 1983,
        education_year  => 27,
        native_language => 'es',
        contact_email   => 'peter@pepper.uk',
        contact_address => 'Bathwater Circle, AB1 2CD, Leicestershireborough',
        contact_phone   => '5678912345',
        date_created    => DateTime->now,
        date_updated    => DateTime->now,
      },
    ],
  });

  my %participants = map { $_->last_name => $_->id }
    $collection->collection_participants->all;

  my $sessions = $db->schema->resultset('CollectionSession');

  $sessions->create({
    collection   => $collection->id,
    date_started => DateTime->new( year => 2006, day => '1', month => '9' ),
    date_ended   => DateTime->new( year => 2006, day => '8', month => '9' ),
    equipment    => 'Portable recorder',
    location     => 1,
    country      => 'gb',
    city         => 'Rye',
    date_created => DateTime->now,
    date_updated => DateTime->now,
    collection_session_items => [ map {{
        collection   => $collection->id,
        participant  => $_->{subject},
        date_started => $_->{date},
        date_ended   => $_->{date},
        type         => 'a--',
        data         => $_->{data},
        date_created => DateTime->now,
        date_updated => DateTime->now,
      }} ({
        subject => $participants{Piper},
        date    => DateTime->new( year => 2006, day => '1', month => '9' ),
        data    => 'Foo',
      },{
        subject => $participants{Piper},
        date    => DateTime->new( year => 2006, day => '1', month => '9' ),
        data    => 'Boo',
      },{
        subject => $participants{Piper},
        date    => DateTime->new( year => 2006, day => '1', month => '9' ),
        data    => 'Goo',
      },{
        subject => $participants{Pepper},
        date    => DateTime->new( year => 2006, day => '2', month => '9' ),
        data    => 'Foo',
      },{
        subject => $participants{Pepper},
        date    => DateTime->new( year => 2006, day => '2', month => '9' ),
        data    => 'Boo',
      },{
        subject => $participants{Pepper},
        date    => DateTime->new( year => 2006, day => '3', month => '9' ),
        data    => 'Goo',
      })
    ]
  });

};
