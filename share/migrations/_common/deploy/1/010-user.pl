use strict;
use warnings;

require MIME::Base64;
use DBIx::Class::Migration::RunScript;
use DateTime;

migrate {
  my $db = shift;
  my $rs = $db->schema->resultset('UserAccount');

  my $salted = salt_password('root');
  my $root = $rs->create({
    name          => 'root',
    first_name    => 'Admin',
    last_name     => '',
    email_address => 'j.atria@sheffield.ac.uk',
    permissions   => 'admin',
    hash_method   => 'bcrypt',
    password      => $salted->hash_base64,
    password_salt => MIME::Base64::encode_base64( $salted->salt ),
    date_created  => DateTime->now,
    date_updated  => DateTime->now,
  });

  $root->create_related( applications => {
    name          => 'root',
    display_name  => 'root',
    description   => 'A dummy root application',
    visibility    => 10,
    namespace     => $root->name,
    date_created  => DateTime->now,
    date_updated  => DateTime->now,
  });

  $salted = salt_password('andre');
  my $andre = $rs->create({
    name          => 'andre',
    first_name    => 'Andre',
    last_name     => 'Coy',
    email_address => 'andre@cloudcast.ac.uk',
    permissions   => 'none',
    hash_method   => 'bcrypt',
    password      => $salted->hash_base64,
    password_salt => MIME::Base64::encode_base64( $salted->salt ),
    date_created  => DateTime->now,
    date_updated  => DateTime->now,
  });

  $salted = salt_password('phil');
  my $phil = $rs->create({
    name          => 'phil',
    first_name    => 'Phil',
    last_name     => 'Green',
    email_address => 'phil@cloudcast.ac.uk',
    permissions   => 'none',
    hash_method   => 'bcrypt',
    password      => $salted->hash_base64,
    password_salt => MIME::Base64::encode_base64( $salted->salt ),
    date_created  => DateTime->now,
    date_updated  => DateTime->now,
  });

  $salted = salt_password('max');
  my $max = $rs->create({
    name          => 'max',
    first_name    => 'Max',
    last_name     => 'Malavasi',
    email_address => 'max@cloudcast.ac.uk',
    permissions   => 'none',
    hash_method   => 'bcrypt',
    password      => $salted->hash_base64,
    password_salt => MIME::Base64::encode_base64( $salted->salt ),
    date_created  => DateTime->now,
    date_updated  => DateTime->now,
  });

  $salted = salt_password('jj');
  my $jj = $rs->create({
    name          => 'jj',
    first_name    => 'JJ',
    last_name     => 'Atria',
    email_address => 'j.atria@sheffield.ac.uk',
    permissions   => 'none',
    hash_method   => 'bcrypt',
    password      => $salted->hash_base64,
    password_salt => MIME::Base64::encode_base64( $salted->salt ),
    date_created  => DateTime->now,
    date_updated  => DateTime->now,
  });

  $root->create_related( user_keys => {
    date_created => DateTime->now,
    key => _random_key(),
  });

  my $tutor = $jj->create_related( applications => {
    name          => 'reading-tutor',
    display_name  => 'Reading Tutor',
    description   => 'An automatic literacy tutor for children',
    visibility    => 10,
    namespace     => $jj->name,
    date_created  => DateTime->now,
    date_updated  => DateTime->now,
    application_users => [
      { member => $jj->id,    role => 'owner' },
      { member => $andre->id, role => 'manager' },
      { member => $phil->id,  role => 'user' },
    ],
  });

  my $candc = $jj->create_related( applications => {
    name          => 'command-control',
    display_name  => 'Command and Control',
    description   => 'Control a smart home with speech commands',
    visibility    => 10,
    namespace     => $jj->name,
    date_created  => DateTime->now,
    date_updated  => DateTime->now,
    application_users => [
      { member => $jj->id,   role => 'owner' },
      { member => $max->id,  role => 'manager' },
      { member => $phil->id, role => 'user' },
    ],
  });

};

sub salt_password {
  my ($password, $cost) = @_;
  $password //= generate_password();
  $cost //= 10;

  require Authen::Passphrase::BlowfishCrypt;

  return Authen::Passphrase::BlowfishCrypt->new(
    cost => $cost,
    salt_random => 1,
    passphrase => $password,
  );
}

sub generate_password {
  require Crypt::HSXKPasswd;
  return Crypt::HSXKPasswd->new(
    preset => 'SECURITYQ',
    preset_overrides => {
      padding_alphabet => [qw{! ?}],
      num_words => 5,
      case_transform => 'CAPITALISE',
      separator_character => q{ },
    },
  )->password;
}

sub _random_key {
  require Math::Random::Secure;
  require Encode::Base58;

  my $number = '1' . join '', map {
    sprintf('%010s', Math::Random::Secure::irand('9' x 11))
  } (0 .. 4);

  return substr(Encode::Base58::encode_base58( $number ), 0, 30);
}
