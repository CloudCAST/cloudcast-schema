use strict;
use warnings;

use DBIx::Class::Migration::RunScript;
use JSON::MaybeXS qw( encode_json );

migrate {
  my $db = shift;

  my ($grammar, $lexicon, $app);

  $grammar = $db->schema->resultset('Grammar')->create({
    data => encode_json({}),
    notes => 'tedlium',
    date_created => DateTime->now,
    date_updated => DateTime->now,
  });

  $lexicon = $db->schema->resultset('Lexicon')->create({
    data => encode_json({
      parameters => {
        'word-syms' => 'data/models/tedlium_nnet_ms_sp_online/words.txt',
      }
    }),
    notes => 'tedlium',
    date_created => DateTime->now,
    date_updated => DateTime->now,
  });

  $db->schema->resultset('GrammarLexicon')
    ->create({ grammar => $grammar->id, lexicon => $lexicon->id });

  $app = $db->schema->resultset('Application')
    ->search({ name => 'root' })
    ->first;

  $db->schema->resultset('RecognitionModel')->create({
    name => 'tedlium',
    notes => 'The first model',
    parameters => encode_json({
      'model'                     => 'data/models/tedlium_nnet_ms_sp_online/final.mdl',
      'mfcc-config'               => 'data/models/tedlium_nnet_ms_sp_online/conf/mfcc.conf',
      'ivector-extraction-config' => 'data/models/tedlium_nnet_ms_sp_online/conf/ivector_extractor.conf',
      'endpoint-silence-phones'   => '1:2:3:4:5:6:7:8:9:10',
      'phone-syms'                => 'data/models/tedlium_nnet_ms_sp_online/phones.txt',
      'word-boundary-file'        => 'data/models/tedlium_nnet_ms_sp_online/word_boundary.int',
    }),
    date_created => DateTime->now,
    date_updated => DateTime->now,
    recognisers => [
      {
        name => 'tedlium',
        type => 'kaldinnet2onlinedecoder',
        grammar => $grammar->id,
        lexicon => $lexicon->id,
        parameters => encode_json({
          'fst' => 'data/models/tedlium_nnet_ms_sp_online/HCLG.fst',
        }),
        training_scheduled => 0,
        date_created => DateTime->now,
        date_updated => DateTime->now,
        decodings => [
          {
            name => 'tedlium',
            parameters => encode_json({
              'use-threaded-decoder'     => 1,
              'max-active'               => '10000',
              'beam'                     => 10.0,
              'lattice-beam'             => 6.0,
              'acoustic-scale'           => 0.083,
              'do-endpointing'           => 1,
              'traceback-period-in-secs' => 0.25,
              'chunk-length-in-secs'     => 0.25,
              'num-nbest'                => 5,
              'do-phone-alignment'       => 1,
              'num-phone-alignment'      => 5,
            }),
            application => $app->id,
            date_created => DateTime->now,
            date_updated => DateTime->now,
          },
        ]
      }
    ],
  });

  $grammar = $db->schema->resultset('Grammar')->create({
    data => encode_json({}),
    notes => 'voxforge',
    date_created => DateTime->now,
    date_updated => DateTime->now,
  });

  $lexicon = $db->schema->resultset('Lexicon')->create({
    data => encode_json({
      parameters => {
        'word-syms' => 'data/models/voxforge/tri2b_mmi_b0.05/words.txt',
      }
    }),
    notes => 'voxforge',
    date_created => DateTime->now,
    date_updated => DateTime->now,
  });

  $db->schema->resultset('RecognitionModel')->create({
    name => 'voxforge',
    notes => 'A simple GMM model',
    parameters => encode_json({
      'model'          => 'data/models/voxforge/tri2b_mmi_b0.05/final.mdl',
      'lda-mat'        => 'data/models/voxforge/tri2b_mmi_b0.05/final.mat',
      'silence-phones' => '1:2:3:4:5',
    }),
    date_created => DateTime->now,
    date_updated => DateTime->now,
    recognisers => [
      {
        name => 'voxforge',
        type => 'onlinegmmdecodefaster',
        grammar => $grammar->id,
        lexicon => $lexicon->id,
        parameters => encode_json({
          'fst' => 'data/models/voxforge/tri2b_mmi_b0.05/HCLG.fst',
        }),
        training_scheduled => 0,
        date_created => DateTime->now,
        date_updated => DateTime->now,
        decodings => [
          {
            name => 'voxforge',
            application => $app->id,
            parameters => encode_json({}),
            date_created => DateTime->now,
            date_updated => DateTime->now,
          },
        ]
      }
    ],
  });

  $app = $db->schema->resultset('Application')
    ->search({ name => 'reading-tutor' })
    ->first;

  $grammar = $db->schema->resultset('Grammar')->create({
    data => encode_json({}),
    notes => 'jamlit',
    date_created => DateTime->now,
    date_updated => DateTime->now,
  });

  $lexicon = $db->schema->resultset('Lexicon')->create({
    data => encode_json({
      parameters => {
        'word-syms' => 'data/lang/words.txt',
      }
    }),
    notes => 'jamlit',
    date_created => DateTime->now,
    date_updated => DateTime->now,
  });

  $db->schema->resultset('RecognitionModel')->create({
    name => 'jamlit',
    notes => 'A Jamaican model',
    parameters => encode_json({
      'model'                     => 'exp/nnet3_combine_BrJm_2000/tdnn/final.mdl',
      'mfcc-config'               => 'conf/mfcc.conf',
      'ivector-extraction-config' => 'conf/ivector_extractor.conf',
      'endpoint-silence-phones'   => '1:2:3:4:5:6:7:8:9:10',
      'phone-syms'                => 'data/lang/phones.txt',
      'word-boundary-file'        => 'data/lang/phones/word_boundary.int',
    }),
    date_created => DateTime->now,
    date_updated => DateTime->now,
    recognisers => [
      {
        name => 'jamlit',
        type => 'kaldinnet2onlinedecoder',
        grammar => $grammar->id,
        lexicon => $lexicon->id,
        parameters => encode_json({
          'fst' => 'exp/tri3_combine_BrJm_2000/graph_Jamaican_test/HCLG.fst',
        }),
        training_scheduled => 0,
        date_created => DateTime->now,
        date_updated => DateTime->now,
        decodings => [
          {
            name => 'jamlit',
            application => $app->id,
            parameters => encode_json({
              'use-threaded-decoder'     => 1,
              'max-active'               => '10000',
              'beam'                     => 10.0,
              'lattice-beam'             => 6.0,
              'acoustic-scale'           => 0.083,
              'do-endpointing'           => 1,
              'traceback-period-in-secs' => 0.25,
              'chunk-length-in-secs'     => 0.25,
              'num-nbest'                => 5,
              'do-phone-alignment'       => 1,
              'num-phone-alignment'      => 5,
            }),
            date_created => DateTime->now,
            date_updated => DateTime->now,
          },
        ]
      }
    ],
  });

  my $decodings = $db->schema->resultset('Decoding');

  $app->update({
    default_decoding => $decodings->search({ name => 'jamlit' })->first->id,
  });

  $app = $db->schema->resultset('Application')
    ->search({ name => 'command-control' })
    ->first;

  $app->update({
    default_decoding => $decodings->search({ name => 'tedlium' })->first->id,
  });

};
