use strict;
use warnings;

use DBIx::Class::Migration::RunScript;
use DDP;

migrate {
  my $schema = shift->schema;

  my $rs = $schema->resultset('Application')->search({ namespace => undef });
  while (my $o = $rs->next) {
    $o->update({
      namespace => $o->owner->name,
    });
  }
};
