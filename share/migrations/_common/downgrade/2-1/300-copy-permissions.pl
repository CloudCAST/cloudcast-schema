use strict;
use warnings;

use DBIx::Class::Migration::RunScript;
use DDP;

migrate {
  my $schema = shift->schema;

  $schema->resultset('UserAccount')
    ->search({ is_admin => 1 })
    ->update({ permissions => 'admin' });

  $schema->resultset('UserAccount')
    ->search({ is_admin => 0 })
    ->update({ permissions => 'user' });
};
