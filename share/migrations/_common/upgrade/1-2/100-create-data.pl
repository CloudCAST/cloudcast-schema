use strict;
use warnings;

use DBIx::Class::Migration::RunScript;
use DDP;

migrate {
  my $schema = shift->schema;

  $schema->resultset('Recogniser')
    ->search({ data_set => undef })
    ->update({ data_set => '{}' });

  $schema->resultset('RecognitionModel')
    ->search({ data => undef })
    ->update({ data => '' });
};
