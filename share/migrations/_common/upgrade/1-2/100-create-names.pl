use strict;
use warnings;

use DBIx::Class::Migration::RunScript;
use DDP;

migrate {
  my $schema = shift->schema;
  my $rs;

  my ($app) = $schema->resultset('Application')->search({ name => 'root' });

  foreach (qw( Grammar Lexicon RecognitionModel )) {
    $rs = $schema->resultset($_)->search({ application => undef });
    while (my $o = $rs->next) {
      $o->update({
        application => $app->id,
        name        => $o->notes,
        visibility  => 0,
      });
    }
  }

  $rs = $schema->resultset('Recogniser')->search({ application => undef });
  while (my $o = $rs->next) {
    $o->update({
      application => $app->id,
      visibility  => 0,
    });
  }
};
