use strict;
use warnings;

use DBIx::Class::Migration::RunScript;
use DDP;

migrate {
  shift->schema->resultset('UserAccount')
    ->search({ permissions => 'admin' })
    ->update({ is_admin => 1 });
};
