#!/usr/bin/env perl

use Test::Most;
use Test::DBIx::Class (
    -schema_class  => 'CloudCAST::Schema',
    -fixture_class => '::Population',
  ),
  'User', 'Application', 'DecodingResult',
  'Meta::AccessLevel' => { -as => 'AccessLevel' };

plan skip_all => 'Test runs for schema version 2'
  if Schema->schema_version != 2;

fixtures_ok ['all_tables'];

ok User->result_source->has_column('is_admin'),
  'User has is_admin';

foreach my $field (qw( recognisers decodings grammars models lexicons )) {
  ok Application->result_source->has_relationship($field),
    "Application has ${field}";
}

foreach my $class (qw( Grammar Lexicon Recogniser RecognitionModel )) {
  my $rs = Schema->resultset($class)->result_source;
  foreach my $field (qw( application name visibility )) {
    ok $rs->has_column($field), "$class has $field";
  }
}

ok DecodingResult->result_source->has_column('decoding'),
  'DecodingResult has decoding';

ok AccessLevel->result_source->has_column('is_admin'),
  'Meta::AccessLevel has is_admin';

ok ! AccessLevel->result_source->has_column('can_write_app'),
  'Meta::AccessLevel does not have can_write_app';

done_testing;
